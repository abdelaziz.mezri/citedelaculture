<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Organismeextern
 *
 * @ORM\Table(name="organismeextern")
 * @ORM\Entity
 */
class Organismeextern
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;





    /**
     * @var string
     *
     * @ORM\Column(name="nomOrgan", type="string", length=255)
     */
    private $nomOrgan;
    /**
     * @var string
     *
     * @ORM\Column(name="nomRespon", type="string", length=255)
     */
    private $nomRespon;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $desciption;
    /**
     * @var string
     * @Assert\NotBlank(message="Ajouter une image jpeg")
     * @Assert\File(mimeTypes={ "image/jpeg" })
     *
     * @ORM\Column(name="image",type="string", length=255)
     */
    private $image;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNomOrgan()
    {
        return $this->nomOrgan;
    }

    /**
     * @param string $nomOrgan
     */
    public function setNomOrgan(string $nomOrgan)
    {
        $this->nomOrgan = $nomOrgan;
    }

    /**
     * @return string
     */
    public function getNomRespon()
    {
        return $this->nomRespon;
    }

    /**
     * @param string $nomRespon
     */
    public function setNomRespon(string $nomRespon)
    {
        $this->nomRespon = $nomRespon;
    }

    /**
     * @return string
     */
    public function getDesciption()
    {
        return $this->desciption;
    }

    /**
     * @param string $desciption
     */
    public function setDesciption(string $desciption)
    {
        $this->desciption = $desciption;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image)
    {
        $this->image = $image;
    }


}

