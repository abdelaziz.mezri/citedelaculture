<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pole
 *
 * @ORM\Table(name="pole", indexes={@ORM\Index(name="idResponsablePole", columns={"idResponsablePole"})})
 * @ORM\Entity
 */
class Pole
{
    /**
     * @var string
     *
     * @ORM\Column(name="idPole", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpole;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idResponsablePole", referencedColumnName="id")
     * })
     */
    private $idresponsablepole;



    /**
     * Get idpole
     *
     * @return string
     */
    public function getIdpole()
    {
        return $this->idpole;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Pole
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set idresponsablepole
     *
     * @param \AppBundle\Entity\User $idresponsablepole
     *
     * @return Pole
     */
    public function setIdresponsablepole(\AppBundle\Entity\User $idresponsablepole = null)
    {
        $this->idresponsablepole = $idresponsablepole;

        return $this;
    }

    /**
     * Get idresponsablepole
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdresponsablepole()
    {
        return $this->idresponsablepole;
    }
}
