<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organisme
 *
 * @ORM\Table(name="organisme", indexes={@ORM\Index(name="idResponsableOrganisme", columns={"idResponsableOrganisme"}), @ORM\Index(name="organisme_ibfk_5", columns={"idPole"})})
 * @ORM\Entity
 */
class Organisme
{
    /**
     * @var string
     *
     * @ORM\Column(name="idOrganisme", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idorganisme;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="blob", nullable=true)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idResponsableOrganisme", referencedColumnName="id")
     * })
     */
    private $idresponsableorganisme;

    /**
     * @var \Pole
     *
     * @ORM\ManyToOne(targetEntity="Pole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idPole", referencedColumnName="idPole")
     * })
     */
    private $idpole;



    /**
     * Get idorganisme
     *
     * @return string
     */
    public function getIdorganisme()
    {
        return $this->idorganisme;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Organisme
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Organisme
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set idresponsableorganisme
     *
     * @param \AppBundle\Entity\User $idresponsableorganisme
     *
     * @return Organisme
     */
    public function setIdresponsableorganisme(\AppBundle\Entity\User $idresponsableorganisme = null)
    {
        $this->idresponsableorganisme = $idresponsableorganisme;

        return $this;
    }

    /**
     * Get idresponsableorganisme
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdresponsableorganisme()
    {
        return $this->idresponsableorganisme;
    }

    /**
     * Set idpole
     *
     * @param \AppBundle\Entity\Pole $idpole
     *
     * @return Organisme
     */
    public function setIdpole(\AppBundle\Entity\Pole $idpole = null)
    {
        $this->idpole = $idpole;

        return $this;
    }

    /**
     * Get idpole
     *
     * @return \AppBundle\Entity\Pole
     */
    public function getIdpole()
    {
        return $this->idpole;
    }
}
