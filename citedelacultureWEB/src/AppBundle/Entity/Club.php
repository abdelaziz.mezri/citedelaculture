<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Club
 *
 * @ORM\Table(name="club", indexes={@ORM\Index(name="idPole", columns={"idPole"}), @ORM\Index(name="club_ibfk_1", columns={"idResponsableClub"})})
 * @ORM\Entity
 */
class Club
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idClub", type="integer", length=11, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idclub;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="cotisationAnnuelle", type="float", precision=10, scale=0, nullable=true)
     */
    private $cotisationannuelle;

    /**
     * @var \Pole
     *
     * @ORM\ManyToOne(targetEntity="Pole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idPole", referencedColumnName="idPole")
     * })
     */
    private $idpole;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idResponsableClub", referencedColumnName="id")
     * })
     */
    private $idresponsableclub;



    /**
     * Get idclub
     *
     * @return string
     */
    public function getIdclub()
    {
        return $this->idclub;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }



    /**
     * Set description
     *
     * @param string $description
     *
     * @return Club
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set cotisationannuelle
     *
     * @param float $cotisationannuelle
     *
     * @return Club
     */
    public function setCotisationannuelle($cotisationannuelle)
    {
        $this->cotisationannuelle = $cotisationannuelle;

        return $this;
    }

    /**
     * Get cotisationannuelle
     *
     * @return float
     */
    public function getCotisationannuelle()
    {
        return $this->cotisationannuelle;
    }

    /**
     * Set idpole
     *
     * @param \AppBundle\Entity\Pole $idpole
     *
     * @return Club
     */
    public function setIdpole(\AppBundle\Entity\Pole $idpole = null)
    {
        $this->idpole = $idpole;

        return $this;
    }

    /**
     * Get idpole
     *
     * @return \AppBundle\Entity\Pole
     */
    public function getIdpole()
    {
        return $this->idpole;
    }

    /**
     * Set idresponsableclub
     *
     * @param \AppBundle\Entity\User $idresponsableclub
     *
     * @return Club
     */
    public function setIdresponsableclub(\AppBundle\Entity\FosUser $idresponsableclub = null)
    {
        $this->idresponsableclub = $idresponsableclub;

        return $this;
    }

    /**
     * Get idresponsableclub
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdresponsableclub()
    {
        return $this->idresponsableclub;
    }
}
