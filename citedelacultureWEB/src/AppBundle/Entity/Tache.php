<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tache
 *
 * @ORM\Table(name="tache", indexes={@ORM\Index(name="idEmploye", columns={"idEmploye"})})
 * @ORM\Entity
 */
class Tache
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idTache", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtache;

    /**
     * @var boolean
     *
     * @ORM\Column(name="termine", type="boolean", nullable=false)
     */
    private $termine;

    /**
     * @var string
     *
     * @ORM\Column(name="texte", type="string", length=150, nullable=false)
     */
    private $texte;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idEmploye", referencedColumnName="id")
     * })
     */
    private $idemploye;



    /**
     * Get idtache
     *
     * @return integer
     */
    public function getIdtache()
    {
        return $this->idtache;
    }

    /**
     * Set termine
     *
     * @param boolean $termine
     *
     * @return Tache
     */
    public function setTermine($termine)
    {
        $this->termine = $termine;

        return $this;
    }

    /**
     * Get termine
     *
     * @return boolean
     */
    public function getTermine()
    {
        return $this->termine;
    }

    /**
     * Set texte
     *
     * @param string $texte
     *
     * @return Tache
     */
    public function setTexte($texte)
    {
        $this->texte = $texte;

        return $this;
    }

    /**
     * Get texte
     *
     * @return string
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * Set idemploye
     *
     * @param \AppBundle\Entity\User $idemploye
     *
     * @return Tache
     */
    public function setIdemploye(\AppBundle\Entity\User $idemploye = null)
    {
        $this->idemploye = $idemploye;

        return $this;
    }

    /**
     * Get idemploye
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdemploye()
    {
        return $this->idemploye;
    }
}
