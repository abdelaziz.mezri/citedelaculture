<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message", indexes={@ORM\Index(name="idDestinataire", columns={"idDestinataire"}), @ORM\Index(name="idEmetteur", columns={"idEmetteur"}), @ORM\Index(name="idVisiteur", columns={"idVisiteurEmetteur"})})
 * @ORM\Entity
 */
class Message
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idMessage", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmessage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lu", type="boolean", nullable=false)
     */
    private $lu;

    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="string", length=255, nullable=false)
     */
    private $objet;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="string", length=255, nullable=false)
     */
    private $contenu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateMessage", type="date", nullable=false)
     */
    private $datemessage;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idEmetteur", referencedColumnName="id")
     * })
     */
    private $idemetteur;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idVisiteurEmetteur", referencedColumnName="id")
     * })
     */
    private $idvisiteuremetteur;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idDestinataire", referencedColumnName="id")
     * })
     */
    private $iddestinataire;



    /**
     * Get idmessage
     *
     * @return integer
     */
    public function getIdmessage()
    {
        return $this->idmessage;
    }

    /**
     * Set lu
     *
     * @param boolean $lu
     *
     * @return Message
     */
    public function setLu($lu)
    {
        $this->lu = $lu;

        return $this;
    }

    /**
     * Get lu
     *
     * @return boolean
     */
    public function getLu()
    {
        return $this->lu;
    }

    /**
     * Set objet
     *
     * @param string $objet
     *
     * @return Message
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;

        return $this;
    }

    /**
     * Get objet
     *
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Message
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set datemessage
     *
     * @param \DateTime $datemessage
     *
     * @return Message
     */
    public function setDatemessage($datemessage)
    {
        $this->datemessage = $datemessage;

        return $this;
    }

    /**
     * Get datemessage
     *
     * @return \DateTime
     */
    public function getDatemessage()
    {
        return $this->datemessage;
    }

    /**
     * Set idemetteur
     *
     * @param \AppBundle\Entity\FosUser $idemetteur
     *
     * @return Message
     */
    public function setIdemetteur(\AppBundle\Entity\FosUser $idemetteur = null)
    {
        $this->idemetteur = $idemetteur;

        return $this;
    }

    /**
     * Get idemetteur
     *
     * @return \AppBundle\Entity\FosUser
     */
    public function getIdemetteur()
    {
        return $this->idemetteur;
    }

    /**
     * Set idvisiteuremetteur
     *
     * @param \AppBundle\Entity\FosUser $idvisiteuremetteur
     *
     * @return Message
     */
    public function setIdvisiteuremetteur(\AppBundle\Entity\FosUser $idvisiteuremetteur = null)
    {
        $this->idvisiteuremetteur = $idvisiteuremetteur;

        return $this;
    }

    /**
     * Get idvisiteuremetteur
     *
     * @return \AppBundle\Entity\FosUser
     */
    public function getIdvisiteuremetteur()
    {
        return $this->idvisiteuremetteur;
    }

    /**
     * Set iddestinataire
     *
     * @param \AppBundle\Entity\FosUser $iddestinataire
     *
     * @return Message
     */
    public function setIddestinataire(\AppBundle\Entity\FosUser $iddestinataire = null)
    {
        $this->iddestinataire = $iddestinataire;

        return $this;
    }

    /**
     * Get iddestinataire
     *
     * @return \AppBundle\Entity\FosUser
     */
    public function getIddestinataire()
    {
        return $this->iddestinataire;
    }
}
