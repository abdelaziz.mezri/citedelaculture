<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Evenement
 *
 * @ORM\Table(name="evenement", indexes={@ORM\Index(name="id_type", columns={"id_type"}), @ORM\Index(name="id_espace", columns={"id_espace"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EvenementRepository")
 * @Vich\Uploadable
 */
class Evenement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idevenement;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=false)
     */
    private $prix;

    /**
     * @var boolean
     *
     * @ORM\Column(name="termine", type="boolean" )
     */
    private $termine;

    /**
     * @var boolean
     *
     * @ORM\Column(name="confirme", type="boolean", nullable=false)
     */
    private $confirme;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $date;

    /**
     * @var Typeevenement
     *
     * @ORM\ManyToOne(targetEntity="Typeevenement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type", referencedColumnName="id")
     * })
     */
    private $Type;

    /**
     * @var Espace
     *
     * @ORM\ManyToOne(targetEntity="Espace")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_espace", referencedColumnName="idEspace")
     * })
     */
    private $Espace;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    public function __construct()
    {
        $this->termine = false ;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getIdevenement()
    {
        return $this->idevenement;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Evenement
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Evenement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Evenement
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set confirme
     *
     * @param boolean $confirme
     *
     * @return Evenement
     */
    public function setConfirme($confirme)
    {
        $this->confirme = $confirme;

        return $this;
    }

    /**
     * Get confirme
     *
     * @return boolean
     */
    public function getConfirme()
    {
        return $this->confirme;
    }

    /**
     * @return Typeevenement
     */
    public function getType()
    {
        return $this->Type;
    }

    /**
     * @param Typeevenement $Type
     */
    public function setType(Typeevenement $Type)
    {
        $this->Type = $Type;
    }

    /**
     * Set Espace
     *
     * @param \AppBundle\Entity\Espace $Espace
     *
     * @return Evenement
     */
    public function setEspace(\AppBundle\Entity\Espace $Espace = null)
    {
        $this->Espace = $Espace;

        return $this;
    }

    /**
     * Get Espace
     *
     * @return \AppBundle\Entity\Espace
     */
    public function getEspace()
    {
        return $this->Espace;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser( $user)
    {
        $this->user = $user;
    }

    // ...

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getJson()
    {
        return json_encode($this->getIdevenement());
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return bool
     */
    public function isTermine(): bool
    {
        return $this->termine;
    }

    /**
     * @param bool $status
     */
    public function setTermine(bool $termine): void
    {
        $this->termine = $termine;
    }

    /**
     * Get termine
     *
     * @return boolean
     */
    public function getTermine()
    {
        return $this->termine;
    }
}
