<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationespace
 *
 * @ORM\Table(name="reservationespace", indexes={@ORM\Index(name="reservationespace_ibfk_1", columns={"idClient"}), @ORM\Index(name="reservationespace_ibfk_2", columns={"idEspace"})})
 * @ORM\Entity
 */
class Reservationespace
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idClient", referencedColumnName="id")
     * })
     */
    private $idclient;

    /**
     * @var \Espace
     *
     * @ORM\ManyToOne(targetEntity="Espace")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idEspace", referencedColumnName="idEspace")
     * })
     */
    private $idespace;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idclient
     *
     * @param \AppBundle\Entity\User $idclient
     *
     * @return Reservationespace
     */
    public function setIdclient(\AppBundle\Entity\User $idclient = null)
    {
        $this->idclient = $idclient;

        return $this;
    }

    /**
     * Get idclient
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdclient()
    {
        return $this->idclient;
    }

    /**
     * Set idespace
     *
     * @param \AppBundle\Entity\Espace $idespace
     *
     * @return Reservationespace
     */
    public function setIdespace(\AppBundle\Entity\Espace $idespace = null)
    {
        $this->idespace = $idespace;

        return $this;
    }

    /**
     * Get idespace
     *
     * @return \AppBundle\Entity\Espace
     */
    public function getIdespace()
    {
        return $this->idespace;
    }
}
