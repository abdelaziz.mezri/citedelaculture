<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Note
 *
 * @ORM\Table(name="note", indexes={@ORM\Index(name="idEmploye", columns={"idEmploye"})})
 * @ORM\Entity
 */
class Note
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idNote", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idnote;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=25, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idEmploye", referencedColumnName="id")
     * })
     */
    private $idemploye;



    /**
     * Get idnote
     *
     * @return integer
     */
    public function getIdnote()
    {
        return $this->idnote;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Note
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Note
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set idemploye
     *
     * @param \AppBundle\Entity\User $idemploye
     *
     * @return Note
     */
    public function setIdemploye(\AppBundle\Entity\User $idemploye = null)
    {
        $this->idemploye = $idemploye;

        return $this;
    }

    /**
     * Get idemploye
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdemploye()
    {
        return $this->idemploye;
    }
}
