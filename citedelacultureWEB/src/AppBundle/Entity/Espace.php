<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Espace
 *
 * @ORM\Table(name="espace")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EspaceRepository")
 * @ORM\Entity
 */
class Espace
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idEspace", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idespace;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="capacite", type="integer", nullable=true)
     */
    private $capacite;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="prixDeLocation", type="float", precision=10, scale=0, nullable=false)
     */
    private $prixdelocation;



    /**
     * Get idespace
     *
     * @return integer
     */
    public function getIdespace()
    {
        return $this->idespace;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Espace
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set capacite
     *
     * @param integer $capacite
     *
     * @return Espace
     */
    public function setCapacite($capacite)
    {
        $this->capacite = $capacite;

        return $this;
    }

    /**
     * Get capacite
     *
     * @return integer
     */
    public function getCapacite()
    {
        return $this->capacite;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Espace
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set prixdelocation
     *
     * @param float $prixdelocation
     *
     * @return Espace
     */
    public function setPrixdelocation($prixdelocation)
    {
        $this->prixdelocation = $prixdelocation;

        return $this;
    }

    /**
     * Get prixdelocation
     *
     * @return float
     */
    public function getPrixdelocation()
    {
        return $this->prixdelocation;
    }
}
