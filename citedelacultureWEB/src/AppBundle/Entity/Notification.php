<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="notification", indexes={@ORM\Index(name="idDestinataire", columns={"idDestinataire"})})
 * @ORM\Entity
 */
class Notification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idNotification", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idnotification;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255, nullable=false)
     */
    private $message;

    /**
     * @var boolean
     *
     * @ORM\Column(name="vu", type="boolean", nullable=true)
     */
    private $vu;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idDestinataire", referencedColumnName="id")
     * })
     */
    private $iddestinataire;



    /**
     * Get idnotification
     *
     * @return integer
     */
    public function getIdnotification()
    {
        return $this->idnotification;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Notification
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set vu
     *
     * @param boolean $vu
     *
     * @return Notification
     */
    public function setVu($vu)
    {
        $this->vu = $vu;

        return $this;
    }

    /**
     * Get vu
     *
     * @return boolean
     */
    public function getVu()
    {
        return $this->vu;
    }

    /**
     * Set iddestinataire
     *
     * @param \AppBundle\Entity\User $iddestinataire
     *
     * @return Notification
     */
    public function setIddestinataire(\AppBundle\Entity\User $iddestinataire = null)
    {
        $this->iddestinataire = $iddestinataire;

        return $this;
    }

    /**
     * Get iddestinataire
     *
     * @return \AppBundle\Entity\User
     */
    public function getIddestinataire()
    {
        return $this->iddestinataire;
    }
}
