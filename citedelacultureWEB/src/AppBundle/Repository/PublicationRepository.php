<?php
/**
 * Created by PhpStorm.
 * User: asus
 * Date: 15/04/2019
 * Time: 20:43
 */

namespace AppBundle\Repository;

class PublicationRepository extends \Doctrine\ORM\EntityRepository
{


    public function recherche($type)
    {
        $query= PublicationRepository::createQueryBuilder('r')
            ->where('r.type = :type ')
            ->setParameter('type',$type)
            ->getQuery();
        return $query->getResult();
    }
    public function recherche2($titre)
    {
        $query= PublicationRepository::createQueryBuilder('r')
            ->where('r.titre = :titre ')
            ->setParameter('titre',$titre)
            ->getQuery();
        return $query->getResult();
    }

    public function recherche3($type,$titre)
    {
        $query= PublicationRepository::createQueryBuilder('r')
            ->where('r.type = :type ')
            ->andWhere('r.titre = :titre ')

            ->setParameters(array('titre'=>$titre,'type'=>$type))
            ->getQuery();
        return $query->getResult();
    }

}