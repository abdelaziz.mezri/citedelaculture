<?php

namespace EvenementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Typeevenement;
use AppBundle\Form\TypeevenementType;

class TypeEvenementController extends Controller
{
    public function ajoutTypeEvenementAction(Request $request)
    {
        $typeevenement= new Typeevenement() ;
        $form = $this->createForm(TypeevenementType::class,$typeevenement);
        $form->handleRequest($request) ;
        if ($form->isSubmitted()&&$form->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $em->persist($typeevenement) ;
            $em->flush();
            return $this->redirectToRoute('affiche_type_evenement');
        }
        return $this->render('@Evenement/TypeEvenement/ajout_type_evenement.html.twig', array(
            'formAjoutTypeEvent'=>$form->createView()
            ,"typeevenement" => $typeevenement
        ));
    }

    public function afficheTypeEvenementAction(Request $request)
    {
        //creer une instance de l'entity manager
        $em = $this->getDoctrine()->getManager();
        $typeevenements = $em->getRepository("AppBundle:Typeevenement")//il faut passer par vue
        ->findAll(); //recuperer tous les modeles

        $paginator = $this->get('knp_paginator');
        $typeevenements = $paginator->paginate(
            $typeevenements, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('@Evenement/TypeEvenement/affiche_type_evenement.html.twig', array(
            "typeevenements" => $typeevenements
        ));
    }

    public function deleteTypeEvenementAction(Request $request, $id)
    {
        $typeevenement= new Typeevenement() ;
        $em = $this->getDoctrine()->getManager();
        $typeevenement = $em->getRepository("AppBundle:Typeevenement")->find($id);
        $em->remove($typeevenement);
        $em->flush();
        return $this->redirectToRoute('affiche_type_evenement');
    }

    public function updateTypeEvenementAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $typeevenement = $em->getRepository("AppBundle:Typeevenement")->find($id);
        $form = $this->createForm(TypeevenementType::class, $typeevenement);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $em->persist($typeevenement);
            $em->flush();
            return $this->redirectToRoute('affiche_type_evenement');
        }
        return $this->render('@Evenement/TypeEvenement/update_type_evenement.html.twig', array(
            'form' => $form->createView()
            ,'typeevenement' => $typeevenement
        ));
    }

}
