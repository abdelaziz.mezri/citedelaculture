<?php

namespace EvenementBundle\Controller;

use AppBundle\Entity\Evenement;
use AppBundle\Form\EvenementType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EvenementController extends Controller
{
    public function afficheevenementAction(Request $request)
    {
        //creer une instance de l'entity manager
        $em = $this->getDoctrine()->getManager();
        $evenements = $em->getRepository("AppBundle:Evenement")//il faut passer par vue
        ->findAll(); //recuperer tous les modeles

        $this->verifDate($evenements,$em);

        $paginator = $this->get('knp_paginator');
        $evenements = $paginator->paginate(
            $evenements, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('@Evenement/Evenement/affiche_evenement.html.twig', array(
            "evenements" => $evenements
        ));
    }

    public function afficheevenementconfirmeAction(Request $request, $confirm)
    {
        //creer une instance de l'entity manager
        $em = $this->getDoctrine()->getManager();
        $evenements = $em->getRepository("AppBundle:Evenement")//il faut passer par vue
        ->findConfirme($confirm); //recuperer tous les modeles

        $this->verifDate($evenements,$em);

        $paginator = $this->get('knp_paginator');
        $evenements = $paginator->paginate(
            $evenements, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('@Evenement/Evenement/affiche_evenement.html.twig', array(
            "evenements" => $evenements
        ));
    }

    public function affichemesevenementsAction(Request $request, $user)
    {
        //creer une instance de l'entity manager
        $em = $this->getDoctrine()->getManager();
        $evenements = $em->getRepository("AppBundle:Evenement")//il faut passer par vue
        ->findByOrganisateur($user); //recuperer tous les modeles

        $this->verifDate($evenements,$em);

        $paginator = $this->get('knp_paginator');
        $evenements = $paginator->paginate(
            $evenements, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('@Evenement/Evenement/affiche_mes_evenements.html.twig', array(
            "evenements" => $evenements
        ));
    }

    public function ajoutevenementAction(Request $request)

    {
        $user = $request->get('user');
        $evenement = new Evenement(); //INSTANCE DE NOTRE CLASSE
        $form = $this->createForm(EvenementType::class, $evenement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $evenement->setConfirme(true);
            $evenement->setTermine(false);
            $evenement->setUser($this->getUser());
            $em->persist($evenement);// elle est rempli grace a create form
            $em->flush();

            return $this->redirectToRoute('affiche_evenement');
        }

        return $this->render('@Evenement/Evenement/ajout_evenement.html.twig', array(
            "form" => $form->createView()
        , "evenement" => $evenement
        ));

    }

    public function proposerevenementAction(Request $request)

    {
        $evenement = new Evenement(); //INSTANCE DE NOTRE CLASSE
        $form = $this->createForm(EvenementType::class, $evenement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $evenement->setConfirme(false);
            $evenement->setTermine(false);
            $evenement->setUser($this->getUser());
            $em->persist($evenement);// elle est rempli grace a create form
            $em->flush();
        }

        return $this->render('@Evenement/Evenement/proposer_evenement.html.twig', array(
            "form" => $form->createView()
        , "evenement" => $evenement
        ));

    }

    public function deleteEvenementAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository("AppBundle:Evenement")->find($id);
        $em->remove($evenement);
        $em->flush();
        if(in_array('ROLE_ADMIN', $this->getUser()->getRoles()))
            return $this->redirectToRoute('affiche_evenement');
        else{
            $em = $this->getDoctrine()->getManager();
        $evenements = $em->getRepository("AppBundle:Evenement")//il faut passer par vue
        ->findByOrganisateur($this->getUser()->getId()); //recuperer tous les modeles

        $this->verifDate($evenements,$em);

        $paginator = $this->get('knp_paginator');
        $evenements = $paginator->paginate(
            $evenements, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('@Evenement/Evenement/affiche_mes_evenements.html.twig', array(
            "evenements" => $evenements
        ));}
    }

    public function confirmEvenementAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository("AppBundle:Evenement")->find($id);
        $evenement->setConfirme(true);
        $em->persist($evenement);
        $em->flush();
        return $this->redirectToRoute('affiche_evenement');
    }

    public function verifDate($evenements,$em){
        $datenow = new \DateTime('now');
        foreach ($evenements as $e)
        {
            if($e->getDate() < $datenow )
            {
                if($e->isTermine()!=true)
                {
                    $e->setTermine(true);
                    $em->persist($e);
                    $em->flush();
                }
            }else
            {
                $e->setTermine(false);
                $em->persist($e);
                $em->flush();
            }

        }
    }

    public function updateEvenementAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository("AppBundle:Evenement")->find($id);
        $form = $this->createForm(EvenementType::class, $evenement);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $em->persist($evenement);
            $em->flush();
            if(in_array('ROLE_ADMIN', $this->getUser()->getRoles()))
                return $this->redirectToRoute('affiche_evenement');
            else{
                $em = $this->getDoctrine()->getManager();
                $evenements = $em->getRepository("AppBundle:Evenement")//il faut passer par vue
                ->findByOrganisateur($this->getUser()->getId()); //recuperer tous les modeles

                $this->verifDate($evenements,$em);

                $paginator = $this->get('knp_paginator');
                $evenements = $paginator->paginate(
                    $evenements, /* query NOT result */
                    $request->query->getInt('page', 1)/*page number*/,
                    10/*limit per page*/
                );
                return $this->render('@Evenement/Evenement/affiche_mes_evenements.html.twig', array(
                    "evenements" => $evenements
                ));}
        }
        return $this->render('@Evenement/Evenement/update_evenement.html.twig', array(
            'form' => $form->createView()
        ,'evenement' => $evenement
        ));
    }

    public function verifEspaceEvenementAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $espace = $request->get('espace_id');
        $places = $request->get('places');
        $entitie = $em->getRepository('AppBundle:Espace')->findOneBy(array('idespace'=>$espace));

        if($entitie->getCapacite()<$places){
            //return  new Response(json_encode($entities));
            return new Response("false");
        }else{
            return new Response("true");
        }

    }

    public function afficheevenementsfrontAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $evenements = $em->getRepository("AppBundle:Evenement")->findBy(array('confirme'=>true));//il faut passer par vue
        //   ->findAll(); //recuperer tous les modeles

        return $this->render('@Evenement/Evenement/affiche_evenement_front.html.twig', array(
            "evenements" => $evenements
        ));
    }

    public function getRealEntities($entities){

        $data = $this->get('serializer')->serialize($entities, 'json');
        return $data;
    }

    public function evenementDetailAjaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('evenement_id');
        $entities = $em->getRepository('AppBundle:Evenement')->find($id);
        if (!$entities) {
            return new Response("false");
        } else {
            $result = $this->getRealEntities($entities);
        }

        $response = new Response($result );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

}
