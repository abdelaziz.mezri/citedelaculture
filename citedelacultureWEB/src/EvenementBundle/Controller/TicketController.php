<?php

namespace EvenementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Ticket;
use AppBundle\Form\TypeevenementType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Controller\Osms;

class TicketController extends Controller
{
    public function afficheticketAction(Request $request)
    {
        //creer une instance de l'entity manager
        $em = $this->getDoctrine()->getManager();
        $tickets = $em->getRepository("AppBundle:Ticket")//il faut passer par vue
        ->findAll(); //recuperer tous les modeles

        $paginator = $this->get('knp_paginator');
        $tickets = $paginator->paginate(
            $tickets, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('@Evenement/Ticket/affiche_ticket.html.twig', array(
            "tickets" => $tickets
        ));
    }
}
