<?php

namespace EvenementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Ticket;
use AppBundle\Form\TypeevenementType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Controller\Osms;

class AvisEvenementController extends Controller
{
    public function afficheavisevenementAction(Request $request)
    {
        //creer une instance de l'entity manager
        $em = $this->getDoctrine()->getManager();
        $avisevenements = $em->getRepository("AppBundle:Avisevenement")//il faut passer par vue
        ->findAll(); //recuperer tous les modeles

        $paginator = $this->get('knp_paginator');
        $avisevenements = $paginator->paginate(
            $avisevenements, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('@Evenement/AvisEvenement/affiche_avis_evenement.html.twig', array(
            "avisevenements" => $avisevenements
        ));
    }

    public function getRealEntities($entities){

        $data = $this->get('serializer')->serialize($entities, 'json');
        return $data;
    }

    public function avisevenementajaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $requestString = $request->get('id');
        $entities = $em->getRepository('AppBundle:AvisEvenement')->findBy(array('Evenement'=>$requestString));
        if (!$entities) {
            return new Response("false");
        } else {
            $result = $this->getRealEntities($entities);
        }

        $response = new Response($result );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
