<?php

namespace frontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@front/Default/index.html.twig');
    }

    public function afficheoffrefrontAction()
    {
        $em = $this->getDoctrine()->getManager();
        $offres = $em->getRepository("AppBundle:Offre")->findBy(array('status'=>true));//il faut passer par vue
        //   ->findAll(); //recuperer tous les modeles
        $this->verifDate($offres,$em);
        return $this->render('@front/offre/afficheoffrefront.html.twig', array(
            "offres" => $offres

        ));

    }

    public function verifDate($offres,$em){
        $datenow = new \DateTime('now');

        foreach ($offres as $e)
        {
            if($e->getDateFin() < $datenow )
            {
                if($e->isStatus()!=false)
                {

                    $e->setStatus(false);
                    $em->persist($e);
                    $em->flush();
                    $event = $e->getEvenement();
                    $event->setPrix($e->getPrixDebut());
                    $em->merge($event);
                    $em->flush();
                }
            }else
            {
                $e->setStatus(true);
                $em->persist($e);
                $em->flush();
            }

        }
    }
}
