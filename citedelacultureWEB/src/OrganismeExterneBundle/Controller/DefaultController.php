<?php

namespace OrganismeExterneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@OrganismeExterne/Default/index.html.twig');
    }
}
