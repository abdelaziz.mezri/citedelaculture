<?php

namespace MobileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use AppBundle\Entity\Publication;

class MobileController extends Controller
{
    public function Publication_allAction()
    {
        $bonplans = $this->getDoctrine()->getManager()->getRepository('AppBundle:Publication')->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($bonplans);
        return new JsonResponse($formatted);

    }

    public function find_PublicationAction($id)
    {
        $bonplans = $this->getDoctrine()->getManager()->getRepository('AppBundle:Publication')->find($id);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($bonplans);
        return new JsonResponse($formatted);

    }
    public function deletePAction($idpublication)
    {

        $em=$this->getDoctrine()->getManager();
        $pub=$em->getRepository("AppBundle:Publication")->find($idpublication);
        $em->remove($pub);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($pub);
        return new JsonResponse($formatted);
    }



}