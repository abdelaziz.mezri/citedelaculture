<?php

namespace WebServiceBundle\Controller;

use AppBundle\Entity\Reclamation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ServiceReclamationController extends Controller
{
    public function ajouterReclamationAction(Request $request)
    {
        $user = $this->getDoctrine()->getManager()->getRepository("AppBundle:User")->find($request->get('idUser'));
        $evenement = $this->getDoctrine()->getManager()->getRepository("AppBundle:Evenement")->find($request->get('idEvenement'));

        $em = $this->getDoctrine()->getManager();
        $reclamation = new Reclamation();

        $reclamation->setDescription($request->get('contenu'));
        $reclamation->setPriorite("");
        $reclamation->setTitre($request->get('titre'));
        $reclamation->setEvenement($evenement);
        $reclamation->setUser($user);

        $em->persist($reclamation);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($reclamation);
        return new JsonResponse($formatted);
    }
}
