<?php

namespace WebServiceBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ServiceUserController extends Controller
{
    public function getUsersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository("AppBundle:User")->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($users);
        return new JsonResponse($formatted);
    }

    public function getUserParIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("AppBundle:User")->find($id);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($user);
        return new JsonResponse($formatted);
    }

    public function getUserParUsernameAction($username)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("AppBundle:User")->findBy(array('username'=>$username));
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($user);
        return new JsonResponse($formatted);
    }

    public function modifierMotDePasseAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("AppBundle:User")->find($request->get('id'));
        $plainPassword = $request->get('password');
        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
        $encoded = $encoder->encodePassword($user, $plainPassword);
        $user->setPassword($encoded);
        $user->setPlainPassword($plainPassword);
        $em->merge($user);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($user);
        return new JsonResponse($formatted);
    }

    public function modifierProfileAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("AppBundle:User")->find($request->get('id'));

        $username = $request->get('username');
        $email = $request->get('email');
        $tel = $request->get('tel');
        $subscribe = $request->get('subscribe');

        $user->setUsername($username);
        $user->setUsernameCanonical($username);
        $user->setEmail($email);
        $user->setEmailCanonical($email);
        $user->setTel($tel);
        if($subscribe=='true')
            $user->setSubscribe(true);
        else
            $user->setSubscribe(false);
        $em->merge($user);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($user);
        return new JsonResponse($formatted);
    }

    public function loginAction(Request $request){
        $user_manager = $this->get('fos_user.user_manager');
        $factory = $this->get('security.encoder_factory');
        $user = $user_manager->findUserByUsername($request->get('username'));
        $serializer = new Serializer([new ObjectNormalizer()]);
        if($user){
            $encoder = $factory->getEncoder($user);
            if($encoder->isPasswordValid($user->getPassword(),$request->get('password'),$user->getSalt())) {
                $formatted = $serializer->normalize($user);
            }
            else{
                $formatted = $serializer->normalize(null);
            }
        }else{
            $formatted = $serializer->normalize(null);
        }
        return new JsonResponse($formatted);
    }
}
