<?php

namespace WebServiceBundle\Controller;

use AppBundle\Entity\Ticket;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ServiceTicketController extends Controller
{
    public function getMesTicketsAction($idUser)
    {
        $tickets = $this->getDoctrine()->getManager()->getRepository("AppBundle:Ticket")->findBy(array('User'=>$idUser));
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($tickets);
        return new JsonResponse($formatted);
    }

    public function reserverTicketAction(Request $request)
    {
        $idUser = $request->get('user');
        $idEvenement = $request->get('evenement');
        $nbPlaces = $request->get('nbPlaces');
        $code = '216'.$idUser.$idEvenement.$nbPlaces;
        $user = $this->getDoctrine()->getManager()->getRepository("AppBundle:User")->find($idUser);
        $evenement = $this->getDoctrine()->getManager()->getRepository("AppBundle:Evenement")->find($idEvenement);
        $ticket=null;
        if($user and $evenement){
            $ticket = new Ticket();
            $ticket->setEvenement($evenement);
            $ticket->setUser($user);
            $ticket->setNbplaces($nbPlaces);
            $ticket->setUpdatedAt(new \DateTime('now'));
            $ticket->setCode($code);
            $em = $this->getDoctrine()->getManager();
            $em->persist($ticket);
            $em->flush();
        }
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($ticket);
        return new JsonResponse($formatted);
    }
}
