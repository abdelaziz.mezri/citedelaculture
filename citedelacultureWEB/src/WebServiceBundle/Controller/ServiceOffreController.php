<?php

namespace WebServiceBundle\Controller;

use AppBundle\Entity\Rating;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ServiceOffreController extends Controller
{
    public function getOffresAction()
    {
        $em = $this->getDoctrine()->getManager();
        $offres = $em->getRepository("AppBundle:Offre")->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($offres);
        return new JsonResponse($formatted);
    }

    public function getDateDebutOffreAction($id){
        $offre = $this->getDoctrine()->getManager()->getRepository("AppBundle:Offre")->find($id);
        $date = $offre->getDateDebut()->format('Y-m-d H:i:s');
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($date);
        return new JsonResponse($formatted);
    }

    public function getDateFinOffreAction($id){
        $offre = $this->getDoctrine()->getManager()->getRepository("AppBundle:Offre")->find($id);
        $date = $offre->getDateFin()->format('Y-m-d H:i:s');
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($date);
        return new JsonResponse($formatted);
    }

    public function getTopOffreAction()
    {
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository("AppBundle:Offre")->findbestOffre();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($offre);
        return new JsonResponse($formatted);
    }

    public function abonnerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getManager()->getRepository("AppBundle:User")->find($request->get('idUser'));
        $tel = $request->get('tel');
        $user->setSubscribe(true);
        $user->setTel($tel);
        $em->merge($user);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($user);
        return new JsonResponse($formatted);
    }

    public function desabonnerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $request->get('idUser');
        $user = $em->getRepository('AppBundle:User')->find($user);
        $user->setSubscribe(false);
        $em->merge($user);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($user);
        return new JsonResponse($formatted);
    }

    public function getRealEntities($entities){
        $data = $this->get('serializer')->serialize($entities, 'json');
        return $data;
    }

    public function ratingAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $offre_id = $request->get('idOffre');
        $user_id = $request->get('idUser');
        $like = $request->get('like');

        if($like == "true"){
            $like= true ;
        }else{
            $like= false ;
        }

        $user = $em->getRepository('AppBundle:User')->find($user_id);
        $offre = $em->getRepository('AppBundle:Offre')->find($offre_id);
        $rating = $em->getRepository('AppBundle:Rating')->findOneBy(array('user'=>$user_id , 'offre'=>$offre_id));

        if (!$rating) {
            $rate = new Rating();
            $rate->setLikes($like);
            $rate->setOffre($offre);
            $rate->setUser($user);
            $em->persist($rate);
            $em->flush();

            if($like == true){
                $offre->setLikesnumber( $offre->getLikesnumber()+1);
            }else{
                $offre->setDislikesnumber( $offre->getDislikesnumber()+1);
            }
            $em->merge($offre);
            $em->flush();

            $result = $this->get('serializer')->serialize($rate, 'json');

        } else {

            if($rating->isLikes() == $like)
            {
                return new Response("false");
            }else{
                if($like == true){
                    $offre->setLikesnumber( $offre->getLikesnumber()+1);
                    $offre->setDislikesnumber( $offre->getDislikesnumber()-1);
                    $rating->setLikes(true);
                }else{
                    $offre->setDislikesnumber( $offre->getDislikesnumber()+1);
                    $offre->setLikesnumber( $offre->getLikesnumber()-1);
                    $rating->setLikes(false);
                }
                $em->merge($rating);
                $em->merge($offre);
                $em->flush();
            }
            $result = $this->getRealEntities($rating);
        }
        $response = new Response($result);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


}
