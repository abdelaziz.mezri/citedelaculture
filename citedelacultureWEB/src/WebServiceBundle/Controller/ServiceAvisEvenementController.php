<?php

namespace WebServiceBundle\Controller;

use AppBundle\Entity\Avisevenement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ServiceAvisEvenementController extends Controller
{
    public function getTousAvisEvenementAction($idEvenement)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository("AppBundle:Evenement")->find($idEvenement);
        $avisEvenements = $em->getRepository("AppBundle:Avisevenement")->findBy(array("Evenement"=>$evenement));
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($avisEvenements);
        return new JsonResponse($formatted);
    }

    public function getMonAvisEvenementAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository("AppBundle:Evenement")->find($request->get('idEvenement'));
        $user = $em->getRepository("AppBundle:User")->find($request->get('idUser'));
        $avisEvenements = $em->getRepository("AppBundle:Avisevenement")->findOneBy(array("Evenement"=>$evenement,"User"=>$user));
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($avisEvenements);
        return new JsonResponse($formatted);
    }

    public function ajouterAvisEvenementAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository("AppBundle:Evenement")->find($request->get('idEvenement'));
        $user = $em->getRepository("AppBundle:User")->find($request->get('idUser'));
        $avisEvenement = $em->getRepository("AppBundle:Avisevenement")->findOneBy(array("Evenement"=>$evenement,"User"=>$user));
        if($avisEvenement==null){
            $avisEvenement = new Avisevenement();
            $valeur = $request->get('valeur');
            $commentaire = $request->get('commentaire');
            $avisEvenement->setEvenement($evenement);
            $avisEvenement->setUser($user);
            $avisEvenement->setValeur($valeur);
            $avisEvenement->setCommentaire($commentaire);
            $em->persist($avisEvenement);
            $em->flush();
        }else{
            $valeur = $request->get('valeur');
            $commentaire = $request->get('commentaire');
            $avisEvenement->setValeur($valeur);
            $avisEvenement->setCommentaire($commentaire);
            $em->merge($avisEvenement);
            $em->flush();
        }
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($avisEvenement);
        return new JsonResponse($formatted);
    }

    public function modifierAvisEvenementAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $avisEvenement = $em->getRepository("AppBundle:Avisevenement")->find($request->get('id'));
        $valeur = $request->get('valeur');
        $commentaire = $request->get('commentaire');
        $avisEvenement->setValeur($valeur);
        $avisEvenement->setCommentaire($commentaire);
        $avisEvenement->setUpdatedAt();
        $em->merge($avisEvenement);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($avisEvenement);
        return new JsonResponse($formatted);
    }
}
