<?php

namespace WebServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ServiceTypeEvenementController extends Controller
{
    public function getTypesEvenementAction()
    {
        $types = $this->getDoctrine()->getManager()->getRepository("AppBundle:Typeevenement")->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($types);
        return new JsonResponse($formatted);
    }

    public function getTypeEvenementParIdAction($id)
    {
        $type = $this->getDoctrine()->getManager()->getRepository("AppBundle:Typeevenement")->find($id);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($type);
        return new JsonResponse($formatted);
    }
}
