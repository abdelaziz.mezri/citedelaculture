<?php

namespace WebServiceBundle\Controller;

use AppBundle\Entity\Evenement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ServiceEvenementsController extends Controller
{
    public function getEvenementsAction()
    {
        $evenements = $this->getDoctrine()->getManager()->getRepository("AppBundle:Evenement")->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($evenements);
        return new JsonResponse($formatted);
    }

    public function getEvenementsConfirmesAction()
    {
        $evenements = $this->getDoctrine()->getManager()->getRepository("AppBundle:Evenement")->findBy(array('confirme'=>true));
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($evenements);
        return new JsonResponse($formatted);
    }

    public function getEvenementsNonConfirmesAction()
    {
        $evenements = $this->getDoctrine()->getManager()->getRepository("AppBundle:Evenement")->findBy(array('confirme'=>false));
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($evenements);
        return new JsonResponse($formatted);
    }

    public function getEvenementParIdAction($id)
    {
        $evenement = $this->getDoctrine()->getManager()->getRepository("AppBundle:Evenement")->find($id);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($evenement);
        return new JsonResponse($formatted);
    }

    public function getEvenementsParOrganisateurAction($idUser)
    {
        $user = $this->getDoctrine()->getManager()->getRepository("AppBundle:User")->find($idUser);
        $evenements = $this->getDoctrine()->getManager()->getRepository("AppBundle:Evenement")->findBy(array('user'=>$user));
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($evenements);
        return new JsonResponse($formatted);
    }

    public function confirmerEvenementAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository("AppBundle:Evenement")->find($id);
        $evenement->setConfirme(true);
        $em->persist($evenement);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($evenement);
        return new JsonResponse($formatted);
    }

    public function getEvenementsParTypeAction($idType)
    {
        $type = $this->getDoctrine()->getManager()->getRepository("AppBundle:Typeevenement")->find($idType);
        $evenements = $this->getDoctrine()->getManager()->getRepository("AppBundle:Evenement")->findBy(array('Type'=>$type));
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($evenements);
        return new JsonResponse($formatted);
    }

    public function proposerEvenementAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = new Evenement();
        $evenement->setNom($request->get('nom'));
        $evenement->setDescription($request->get('description'));
        $evenement->setPrix($request->get('prix'));
        $evenement->setConfirme(false);
        $date = new \DateTime($request->get('date'));
        $evenement->setDate($date);
        $evenement->setTermine(false);
        $type = $this->getDoctrine()->getManager()->getRepository("AppBundle:TypeEvenement")->find($request->get('idType'));
        $evenement->setType($type);
        $espace = $this->getDoctrine()->getManager()->getRepository("AppBundle:Espace")->find($request->get('idEspace'));
        $evenement->setEspace($espace);
        $user = $this->getDoctrine()->getManager()->getRepository("AppBundle:User")->find($request->get('idUser'));
        $evenement->setUser($user);
        $evenement->setImage($request->get('image'));
        $em->persist($evenement);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($evenement);
        return new JsonResponse($formatted);
    }

    public function getEvenementsParMotCleAction($mot)
    {
        $evenements = $this->getDoctrine()->getManager()->getRepository("AppBundle:Evenement")->findByMotCle($mot);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($evenements);
        return new JsonResponse($formatted);
    }

    public function getDateEvenementAction($id){
        $evenement = $this->getDoctrine()->getManager()->getRepository("AppBundle:Evenement")->find($id);
        $date = $evenement->getDate()->format('Y-m-d H:i:s');
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($date);
        return new JsonResponse($formatted);
    }
}
