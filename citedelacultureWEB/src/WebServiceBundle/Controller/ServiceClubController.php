<?php

namespace WebServiceBundle\Controller;

use AppBundle\AppBundle\Pole;
use AppBundle\Entity\Club;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ServiceClubController extends Controller
{
    public function getClubsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $clubs = $em->getRepository("AppBundle:Club")->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($clubs);
        return new JsonResponse($formatted);
    }

    public function getClubsParIdAction($id)
    {
        $clubs = $this->getDoctrine()->getManager()->getRepository("AppBundle:Club")->find($id);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($clubs);
        return new JsonResponse($formatted);
    }
    public function ajouterClubAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("AppBundle:User")->find($request->get('idUser'));


            $nom = $request->get('nom');
            $description = $request->get('description');
            $cotisationannuelle = $request->get('cotisationannuelle');
            $club = new Club();
            $club->setIdresponsableclub($user);
            $club->setNom($nom);
            $club->setDescription($description);
            $club->setCotisationannuelle($cotisationannuelle);

            $em->persist($club);
            $em->flush();

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($club);
        return new JsonResponse($formatted);
    }

    public function abonnerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getManager()->getRepository("AppBundle:User")->find($request->get('idUser'));
        $tel = $request->get('tel');
        $user->setSubscribe(true);
        $user->setTel($tel);
        $em->merge($user);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($user);
        return new JsonResponse($formatted);
    }

    public function desabonnerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $request->get('idUser');
        $user = $em->getRepository('AppBundle:User')->find($user);
        $user->setSubscribe(false);
        $em->merge($user);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($user);
        return new JsonResponse($formatted);
    }
}
