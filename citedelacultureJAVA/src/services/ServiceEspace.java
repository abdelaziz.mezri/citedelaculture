/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Espace;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import utiles.ConnDB;

/**
 *
 * @author Mezri
 */
public class ServiceEspace {

    /*Connection conn = ConnDB.getInstance().getConn();

    public void ajouterEspace(Espace espace) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "INSERT INTO `espace` (`idEspace`, `description`, `capacite`, `type`, `prixDeLocation`) VALUES ('" + espace.getIdEspace() + "', '" + espace.getDescription() + "', '" + espace.getCapacite() + "', '" + espace.getType() + "', '" + espace.getPrixDeLocation() + "')";
            st.executeUpdate(req);
            espace.getListImagesEspace().forEach(i -> new ServiceImage().ajouterImageEspace(i, espace.getIdEspace()));
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void modifierEspace(Espace espace, Espace nouveauEspace) {
        this.supprimerEspace(espace.getIdEspace());
        this.ajouterEspace(nouveauEspace);
    }

    public void supprimerEspace(String idEspace) {
        try {
            String req = "delete from Espace where idEspace=?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setString(1, idEspace);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public Espace getEspaceSansImages(String idEspace) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "select * from Espace where idEspace = '" + idEspace + "'";
            ResultSet rs = st.executeQuery(req);
            if (rs.next()) {
                return new Espace(idEspace, rs.getString(2), rs.getInt(3), rs.getString(4), rs.getFloat(5));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public Espace getEspace(String idEspace) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "select * from Espace where idEspace = '" + idEspace + "'";
            ResultSet rs = st.executeQuery(req);
            if (rs.next()) {
                return new Espace(idEspace, rs.getString(2), rs.getInt(3), rs.getString(4), rs.getFloat(5), new ServiceImage().getImagesParEspace(idEspace));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public List<Espace> getEspaceParCapacite(int capacite) {
        Statement st;
        List<Espace> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Espace where capacite >= '" + capacite + "'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Espace(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getFloat(5), new ServiceImage().getImagesParEspace(rs.getString(1))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public List<Espace> getEspaces() {
        Statement st;
        List<Espace> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Espace";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Espace(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getFloat(5), new ServiceImage().getImagesParEspace(rs.getString(1))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public List<Espace> getEspacesParType(String type) {
        Statement st;
        List<Espace> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Espace where type = '" + type + "'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Espace(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getFloat(5), new ServiceImage().getImagesParEspace(rs.getString(1))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public List<Espace> getEspacesParMotCle(String motCle) {
        Statement st;
        List<Espace> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Espace where idEspace like '%" + motCle + "%' or description like '%" + motCle + "%'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Espace(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getFloat(5), new ServiceImage().getImagesParEspace(rs.getString(1))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }*/
}
