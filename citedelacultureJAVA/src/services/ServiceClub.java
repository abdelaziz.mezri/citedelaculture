/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Club;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import utiles.ConnDB;

/**
 *
 * @author Ahmed
 */
public class ServiceClub {

    /*Connection conn = ConnDB.getInstance().getConn();

    public void ajouterClub(Club club) {
        Statement st;
        String req = "";
        
            req = "INSERT INTO `Club` (`idClub`, `description`, `cotisationAnnuelle`, `idLogo`, `idPole`, `idResponsableClub`) VALUES ('" + club.getIdClub() + "', '" + club.getDescription() + "', '" + club.getCotisationAnnuelle() + "', null, '" + club.getPole().getIdPole() + "', '" + club.getResponsableClub().getId() + "')";
        
        try {
            st = conn.createStatement();
            st.executeUpdate(req);
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void modifierClub(String idClub, String description, Double cotisationAnnuelle, File logo, String idPole, String idResponsableClub) {
        
        try {
            String req = "update Club set description=?, cotisationAnnuelle=?, idPole=?, idResponsableClub=? where idClub=?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setString(1, description);
            pt.setDouble(2, cotisationAnnuelle);
            pt.setString(3, idPole);
            pt.setString(4, idResponsableClub);
            pt.setString(5, idClub);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void supprimerClub(String idClub) {
        try {
            
            
            String req = "delete from Club where idClub=?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setString(1, idClub);
            pt.executeUpdate();
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public Club getClub(String idClub) {
        Statement st;
        ServiceUtilisateur su = new ServiceUtilisateur();
        
        ServicePole sp = new ServicePole();
        try {
            st = conn.createStatement();
            String req = "select * from Club where idClub = '" + idClub + "'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                return new Club(rs.getString(1), rs.getString(5), rs.getDouble(6), sp.getPole(rs.getString(3)), (Employe) su.getUtilisateur(rs.getString(4)));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public List<Club> getClubs() {
        Statement st;
        List<Club> ls = new ArrayList();
        ServiceUtilisateur su = new ServiceUtilisateur();
        
        ServicePole sp = new ServicePole();
        try {
            st = conn.createStatement();
            String req = "select * from club";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Club(rs.getString(1), rs.getString(5), rs.getDouble(6), sp.getPole(rs.getString(3)), (Employe) su.getUtilisateur(rs.getString(4))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public List<Club> getClubsParPole(String idPole) {
        Statement st;
        List<Club> ls = new ArrayList();
        ServiceUtilisateur su = new ServiceUtilisateur();
        
        ServicePole sp = new ServicePole();
        try {
            st = conn.createStatement();
            String req = "select * from club where idPole=" + idPole;
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Club(rs.getString(1), rs.getString(5), rs.getDouble(6), sp.getPole(idPole), (Employe) su.getUtilisateur(rs.getString(4))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public Club getClubsParResponsableClub(String idResponsableClub) {
        Statement st;
        ServiceUtilisateur su = new ServiceUtilisateur();
        
        ServicePole sp = new ServicePole();
        try {
            st = conn.createStatement();
            String req = "select * from club where idResponsableClub='" + idResponsableClub + "'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                return new Club(rs.getString(1), rs.getString(5), rs.getDouble(6),  sp.getPole(rs.getString(3)), (Employe) su.getUtilisateur(idResponsableClub));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public List<Club> RechercherClubsParMotCle(String motCle) {
        Statement st;
        List<Club> ls = new ArrayList();
        ServiceUtilisateur su = new ServiceUtilisateur();
        
        ServicePole sp = new ServicePole();
        try {
            st = conn.createStatement();
            String req = "select * from club where idClub like '%" + motCle + "%' or description like '%" + motCle + "%'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Club(rs.getString(1), rs.getString(5), rs.getDouble(6), si.getImageParTitre(rs.getString(2)), sp.getPole(rs.getString(3)), (Employe) su.getUtilisateur(rs.getString(4))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }*/
}
