/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Rating;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import utiles.ConnDB;

/**
 *
 * @author Mezri
 */
public class ServiceRating {

    /*Connection conn = ConnDB.getInstance().getConn();

    public void ajouterRating(Rating rating) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "INSERT INTO `rating` (`idEvenement`, `idVisiteur`, `valeur`, `commentaire`) VALUES ('" + rating.getEvenement().getIdEvenement() + "', '" + rating.getVisiteur().getIdVisiteur() + "', '" + rating.getValeur() + "', '" + rating.getCommentaire() + "')";
            st.executeUpdate(req);
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void supprimerRating(String idEvenement, String idVisiteur) {
        try {
            String req = "delete from rating where idEvenement=? and idVisiteur=?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setString(1, idEvenement);
            pt.setString(2, idVisiteur);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void modifierRating(String idEvenement, String idVisiteur, int valeur, String commentaire) {
        try {
            String req = "update rating set valeur=?, commentaire=? where idEvenement=? and idVisiteur=?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setString(3, idEvenement);
            pt.setString(4, idVisiteur);
            pt.setInt(1, valeur);
            pt.setString(2, commentaire);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public List<Rating> getRatings() {
        Statement st;
        List<Rating> ls = new ArrayList();
        ServiceEvenement se = new ServiceEvenement();
        ServiceVisiteur sv = new ServiceVisiteur();
        try {
            st = conn.createStatement();
            String req = "select * from rating";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Rating(rs.getInt(3), rs.getString(4), se.getEvenement(rs.getString(1)), sv.getVisiteur(rs.getString(2))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public List<Rating> getRatingParEvenement(String idEvenement) {
        Statement st;
        List<Rating> ls = new ArrayList();
        ServiceVisiteur sv = new ServiceVisiteur();
        try {
            st = conn.createStatement();
            String req = "select * from rating where idEvenement = '" + idEvenement + "'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Rating(rs.getInt(3), rs.getString(4), new ServiceEvenement().getEvenement(rs.getString(1)), sv.getVisiteur(rs.getString(2))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public int getMoyRatingParEvenement(String idEvenement) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "select AVG(valeur) from rating where idEvenement = '" + idEvenement + "'";
            ResultSet rs = st.executeQuery(req);
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return 0;
    }

    public List<Rating> getRatingParVisiteur(String idVisiteur) {
        Statement st;
        List<Rating> ls = new ArrayList();
        ServiceEvenement se = new ServiceEvenement();
        try {
            st = conn.createStatement();
            String req = "select * from rating where idVisiteur = '" + idVisiteur + "'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Rating(rs.getInt(3), rs.getString(4), se.getEvenement(rs.getString(1)), new ServiceVisiteur().getVisiteur(rs.getString(2))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public Rating getRatingEvenementParVisiteur(String idVisiteur, String idEvenement) {
        Statement st;
        Rating rating = null;
        ServiceEvenement se = new ServiceEvenement();
        try {
            st = conn.createStatement();
            String req = "select * from rating where idVisiteur = '" + idVisiteur + "' and idEvenement = '" + idEvenement + "'";
            ResultSet rs = st.executeQuery(req);
            if (rs.next()) {
                return new Rating(rs.getInt(3), rs.getString(4), se.getEvenement(rs.getString(1)), new ServiceVisiteur().getVisiteur(rs.getString(2)));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return rating;
    }*/
}
