/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.*;
import entities.Utilisateur;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import utiles.ConnDB;

/**
 *
 * @author Dell
 */
public class ServiceUtilisateur {

    Utilisateur gUser;

    Connection conn = ConnDB.getInstance().getConn();

    public Utilisateur getUtilisateur(String idUtilisateur) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "select * from Utilisateur where idUtilisateur = '" + idUtilisateur + "'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                //return new Utilisateur(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10));
            }

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    /*public void ajouterUtilisateur(Utilisateur utilisateur) {
        Statement st;
        String req = "";
        String type = utilisateur.getClass().getSimpleName();
        
            req = "INSERT INTO `utilisateur` VALUES ('" + utilisateur.getIdUtilisateur() + "', null, '" + utilisateur.getMotDePasse() + "', '" + utilisateur.getCin() + "', '" + utilisateur.getNom() + "', '" + utilisateur.getPrenom() + "', '" + utilisateur.getEmail() + "', '" + utilisateur.getNumTel() + "', '" + utilisateur.getAdresse() + "', '" + utilisateur.getDateDeNaissance() + "', '" + type + "')";
        
        try {
            st = conn.createStatement();
            st.executeUpdate(req);
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void modifierUtilisateurParUtilisateur(String idUtilisateur, String motDePasse, String email, String numTel, String adresse) {
        try {
            String req = "update Utilisateur set motDePasse=?, email=? , numTel=? ,adresse=?  where idUtilisateur=?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setString(1, motDePasse);
            pt.setString(2, email);
            pt.setString(3, numTel);
            pt.setString(4, adresse);
            pt.setString(5, idUtilisateur);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void modifierUtilisateurParAdministrateur(String idUtilisateur, String motDePasse, String cin, String nom, String prenom, String email, String numTel, String adresse, Date dateDeNaissance, File image) {
        try {
            
            String req = "UPDATE `utilisateur` SET `motDePasse` =?, `cin` =?, `nom` =?, `prenom` =?, `email` =?, `numTel` =?, `adresse` =?, `dateDeNaissance` =?, `idImage` =? WHERE `idUtilisateur` =?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setString(1, motDePasse);
            pt.setString(2, cin);
            pt.setString(3, nom);
            pt.setString(4, prenom);
            pt.setString(5, email);
            pt.setString(6, numTel);
            pt.setString(7, adresse);
            pt.setDate(8, dateDeNaissance);
            pt.setString(9, "Utilisateur" + idUtilisateur);
            pt.setString(10, idUtilisateur);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void supprimerUtilisateur(String idUtilisateur) {
        try {
            String req = "delete from Utilisateur where idUtilisateur=?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setString(1, idUtilisateur);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public Utilisateur getUtilisateur(String idUtilisateur) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "select * from Utilisateur where idUtilisateur = '" + idUtilisateur + "'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                String type = rs.getString(11);
                switch (type) {
                    case "Employe":
                        return new Employe(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10));
                    case "ResponsablePole":
                        return new ResponsablePole(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10));
                    case "ResponsableClub":
                        return new ResponsableClub(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), new ServiceClub().getClubsParResponsableClub(idUtilisateur));
                    case "ResponsableOrganismeExterne":
                        return new ResponsableOrganismeExterne(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2)), new ServiceOrganisme().getOrganismeExterneParResponsable(idUtilisateur));
                    case "MembreClub":
                        return new MembreClub(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2)));
                    default:
                        break;
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public List<Utilisateur> getUtilisateurs() {
        Statement st;
        List<Utilisateur> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Utilisateur";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                String type = rs.getString(11);
                switch (type) {
                    case "Employe":
                        ls.add(new Employe(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2))));
                        break;
                    case "ResponsablePole":
                        ls.add(new ResponsablePole(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2))));
                        break;
                    case "ResponsableClub":
                        ls.add(new ResponsableClub(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2)), new ServiceClub().getClubsParResponsableClub(rs.getString(1))));
                        break;
                    case "ResponsableOrganismeExterne":
                        ls.add(new ResponsableOrganismeExterne(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2)), new ServiceOrganisme().getOrganismeExterneParResponsable(rs.getString(1))));
                        break;
                    case "MembreClub":
                        ls.add(new MembreClub(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2))));
                        break;
                    default:
                        break;
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public List<Utilisateur> getUtilisateursParMotCle(String motCle) {
        Statement st;
        List<Utilisateur> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Utilisateur where idUtilisateur like '%" + motCle + "%' or cin like '%" + motCle + "%' or nom like '%" + motCle + "%' or prenom like '%" + motCle + "%' or email like '%" + motCle + "%' or numTel like '%" + motCle + "%' or adresse like '%" + motCle + "%' or dateDeNaissance like '%" + motCle + "%' type like '%" + motCle + "%'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                String type = rs.getString(11);
                switch (type) {
                    case "Employe":
                        ls.add(new Employe(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2))));
                        break;
                    case "ResponsablePole":
                        ls.add(new ResponsablePole(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2))));
                        break;
                    case "ResponsableClub":
                        ls.add(new ResponsableClub(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2)), new ServiceClub().getClubsParResponsableClub(rs.getString(1))));
                        break;
                    case "ResponsableOrganismeExterne":
                        ls.add(new ResponsableOrganismeExterne(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2)), new ServiceOrganisme().getOrganismeExterneParResponsable(rs.getString(1))));
                        break;
                    case "MembreClub":
                        ls.add(new MembreClub(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2))));
                        break;
                    default:
                        break;
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    

    public List<ResponsablePole> getResponsablesPoles() {
        Statement st;
        List<ResponsablePole> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Utilisateur where type = 'ResponsablePole'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new ResponsablePole(rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getDate(10), si.getImageParTitre(rs.getString(2))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }*/
}
