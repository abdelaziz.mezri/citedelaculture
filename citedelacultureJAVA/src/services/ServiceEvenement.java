/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Espace;
import entities.Evenement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import utiles.ConnDB;

/**
 *
 * @author asus
 */
public class ServiceEvenement {

    Connection conn = ConnDB.getInstance().getConn();

    /*public void ajouterEvenement(Evenement evenement) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "";
            if (evenement.getClass().equals(Evenement.class)) {
                req = "INSERT INTO `evenement` (`idEvenement`, `idEspace`, `description`, `nationalite`, `nbMaxPlaces`, `prix`, `confirme`, `date`, `temps`, `type`) VALUES ('"
                        + evenement.getIdEvenement() + "', '"
                        + evenement.getEspace().getIdEspace() + "', '"
                        + evenement.getDescription() + "', '"
                        + evenement.getNationalite() + "', '"
                        + evenement.getNbMaxPlaces() + "', '"
                        + evenement.getPrix() + "', '"
                        + "1" + "', '"
                        + evenement.getDate() + "', '"
                        + evenement.getTemps() + "', '"
                        + "Evenement" + "')";
            } else if (evenement.getClass().equals(Spectacle.class)) {
                req = "INSERT INTO `evenement` (`idEvenement`, `idEspace`, `description`, `nationalite`, `nbMaxPlaces`, `prix`, `confirme`, `date`, `temps`, `genre`, `acteurs`, `lienTrailer`, `type`) VALUES ('"
                        + ((Spectacle) evenement).getIdEvenement() + "', '"
                        + ((Spectacle) evenement).getEspace().getIdEspace() + "', '"
                        + ((Spectacle) evenement).getDescription() + "', '"
                        + ((Spectacle) evenement).getNationalite() + "', '"
                        + ((Spectacle) evenement).getNbMaxPlaces() + "', '"
                        + ((Spectacle) evenement).getPrix() + "', '"
                        + "1" + "', '"
                        + ((Spectacle) evenement).getDate() + "', '"
                        + ((Spectacle) evenement).getTemps() + "', '"
                        + ((Spectacle) evenement).getGenre() + "', '"
                        + ((Spectacle) evenement).getActeurs() + "', '"
                        + ((Spectacle) evenement).getLienTrailer() + "', '"
                        + "Spectacle" + "')";
            }
            st.executeUpdate(req);
            evenement.getAffiches().forEach(i -> new ServiceImage().ajouterImageEvenement(i, evenement.getIdEvenement()));
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void proposerEvenement(Evenement evenement) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "";
            if (evenement.getClass().equals(Evenement.class)) {
                req = "INSERT INTO `evenement` (`idEvenement`, `idPoleOrganisateur`, `description`, `nationalite`, `nbMaxPlaces`, `prix`, `confirme`, `date`, `temps`, `type`) VALUES ('"
                        + evenement.getIdEvenement() + "', '"
                        + evenement.getPoleOrganisateur().getIdPole() + "', '"
                        + evenement.getDescription() + "', '"
                        + evenement.getNationalite() + "', '"
                        + evenement.getNbMaxPlaces() + "', '"
                        + evenement.getPrix() + "', '"
                        + "0" + "', '"
                        + evenement.getDate() + "', '"
                        + evenement.getTemps() + "', '"
                        + "Evenement" + "')";
            } else if (evenement.getClass().equals(Spectacle.class)) {
                req = "INSERT INTO `evenement` (``idEvenement`, idPoleOrganisateur`, `description`, `nationalite`, `nbMaxPlaces`, `prix`, `confirme`, `date`, `temps`, `genre`, `acteurs`, `lienTrailer`, `type`) VALUES ('"
                        + ((Spectacle) evenement).getIdEvenement() + "', '"
                        + ((Spectacle) evenement).getPoleOrganisateur().getIdPole() + "', '"
                        + ((Spectacle) evenement).getDescription() + "', '"
                        + ((Spectacle) evenement).getNationalite() + "', '"
                        + ((Spectacle) evenement).getNbMaxPlaces() + "', '"
                        + ((Spectacle) evenement).getPrix() + "', '"
                        + "0" + "', '"
                        + ((Spectacle) evenement).getDate() + "', '"
                        + ((Spectacle) evenement).getTemps() + "', '"
                        + ((Spectacle) evenement).getGenre() + "', '"
                        + ((Spectacle) evenement).getActeurs() + "', '"
                        + ((Spectacle) evenement).getLienTrailer() + "', '"
                        + "Spectacle" + "')";
            }
            st.executeUpdate(req);
            evenement.getAffiches().forEach(i -> new ServiceImage().ajouterImageEvenement(i, evenement.getIdEvenement()));
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void modifierEvenement(Evenement evenement, Evenement nouveauEvenement) {
        this.supprimerEvenement(evenement.getIdEvenement());
        this.ajouterEvenement(nouveauEvenement);
    }*/

    public void confirmerEvenement(int idEvenement) {
        try {
            String req = "update evenement set confirme=1 where id=?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setInt(1, idEvenement);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    public void supprimerEvenement(String idEvenement) {
        try {
            String req = "delete from Evenement where id=?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setString(1, idEvenement);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public Evenement getEvenement(String idEvenement) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "select * from Evenement where id = '" + idEvenement + "'";
            ResultSet rs = st.executeQuery(req);
            if (rs.next()) {
                return new Evenement(rs.getInt(1), rs.getString(5), rs.getBoolean(9));

            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    /*
    public List<Evenement> getEvenements() {
        Statement st;
        List<Evenement> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Evenement";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                if (rs.getString(13).equals("Evenement")) {
                    ls.add(new Evenement(rs.getString(1), rs.getString(4), rs.getString(5), rs.getInt(14), rs.getDouble(6), rs.getBoolean(7), rs.getDate(8), rs.getTime(9), new ServicePole().getPole(rs.getString(2)), new ServiceEspace().getEspace(rs.getString(3)), new ServiceImage().getImagesParEvenement(rs.getString(1))));
                } else if (rs.getString(13).equals("Spectacle")) {
                    ls.add(new Spectacle(rs.getString(1), rs.getString(4), rs.getString(5), rs.getInt(14), rs.getDouble(6), rs.getBoolean(7), rs.getDate(8), rs.getTime(9), new ServicePole().getPole(rs.getString(2)), new ServiceEspace().getEspace(rs.getString(3)), new ServiceImage().getImagesParEvenement(rs.getString(1)), rs.getString(10), rs.getString(11), rs.getString(12)));
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }*/
    public List<Evenement> getSimpleEvenements() {
        Statement st;
        List<Evenement> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Evenement";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Evenement(rs.getInt(1), rs.getString(5), rs.getBoolean(9)));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    /*
    public List<Evenement> getEvenementsConfirmes() {
        Statement st;
        List<Evenement> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Evenement where confirme = true";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                if (rs.getString(13).equals("Evenement")) {
                    ls.add(new Evenement(rs.getString(1), rs.getString(4), rs.getString(5), rs.getInt(14), rs.getDouble(6), rs.getBoolean(7), rs.getDate(8), rs.getTime(9), new ServicePole().getPole(rs.getString(2)), new ServiceEspace().getEspace(rs.getString(3)), new ServiceImage().getImagesParEvenement(rs.getString(1)), new ServiceRating().getRatingParEvenement(rs.getString(1))));
                } else if (rs.getString(13).equals("Spectacle")) {
                    ls.add(new Spectacle(rs.getString(1), rs.getBoolean(7), rs.getDate(8), rs.getTime(9), new ServicePole().getPole(rs.getString(2)), new ServiceEspace().getEspace(rs.getString(3))));
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }
     */
    public List<Evenement> getSimpleEvenementsConfirmes() {
        Statement st;
        List<Evenement> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Evenement where confirme = true";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Evenement(rs.getInt(1), rs.getString(5), rs.getBoolean(9)));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    /*
    public List<Evenement> getEvenementsNonConfirmes() {
        Statement st;
        List<Evenement> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Evenement where confirme = false";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                if (rs.getString(13).equals("Evenement")) {
                    ls.add(new Evenement(rs.getString(1), rs.getString(4), rs.getString(5), rs.getInt(14), rs.getDouble(6), rs.getBoolean(7), rs.getDate(8), rs.getTime(9), new ServicePole().getPole(rs.getString(2)), new ServiceEspace().getEspace(rs.getString(3)), new ServiceImage().getImagesParEvenement(rs.getString(1)), new ServiceRating().getRatingParEvenement(rs.getString(1))));
                } else if (rs.getString(13).equals("Spectacle")) {
                    ls.add(new Spectacle(rs.getString(1), rs.getBoolean(7), rs.getDate(8), rs.getTime(9), new ServicePole().getPole(rs.getString(2)), new ServiceEspace().getEspace(rs.getString(3))));
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }
     */
    public List<Evenement> getSimpleEvenementsNonConfirmes() {
        Statement st;
        List<Evenement> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Evenement where confirme = false";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Evenement(rs.getInt(1), rs.getString(5), rs.getBoolean(9)));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public List<Evenement> getEvenementsParMotCle(String motCle) {
        Statement st;
        List<Evenement> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Evenement where id like '%" + motCle + "%' or description like '%" + motCle + "%' or date like '%" + motCle + "%'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Evenement(rs.getInt(1), rs.getString(5), rs.getBoolean(9)));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public List<Evenement> getSimpleEvenementsParMotCle(String motCle) {
        Statement st;
        List<Evenement> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Evenement where id like '%" + motCle + "%' or description like '%" + motCle + "%' or date like '%" + motCle + "%'";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                ls.add(new Evenement(rs.getInt(1), rs.getString(5), rs.getBoolean(9)));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    /*
    public List<Evenement> getEvenementsParType(String type) {
        Statement st;
        List<Evenement> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Evenement where type = '" + type + "'";
            ResultSet rs = st.executeQuery(req);
            if (rs.next()) {
                if (rs.getString(3)!=null) {
                    ls.add(new Evenement(rs.getString(1), rs.getString(4), rs.getDouble(5), rs.getString(6), rs.getString(7), rs.getDate(8), rs.getDate(9), new ServicePole().getPole(rs.getString(3)), new ServiceEspace().getEspace(rs.getString(2)), new ServiceImage().getImagesParEvenement(rs.getString(1))));
                }
                else if (rs.getString(4)!=null) {
                    ls.add(new Evenement(rs.getString(1), rs.getString(4), rs.getDouble(5), rs.getString(6), rs.getString(7), rs.getDate(8), rs.getDate(9), new ServiceOrganisme().getOrganisme(rs.getString(4)), new ServiceEspace().getEspace(rs.getString(2)), new ServiceImage().getImagesParEvenement(rs.getString(1))));
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }
     */
}
