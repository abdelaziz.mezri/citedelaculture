/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Pole;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import utiles.ConnDB;

/**
 *
 * @author Ahmed
 */
public class ServicePole {

    /*Connection conn = ConnDB.getInstance().getConn();

    public void ajouterPole(Pole Pole) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "INSERT INTO `Pole` (`idResponsablePole`,`idPole`,`description`) VALUES ('" + Pole.getResponsablePole().getIdUtilisateur() + "', '" + Pole.getIdPole() + "', '" + Pole.getDescription() + "')";
            st.executeUpdate(req);
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void modifierPole(String idPole, String description, String idResponsablePole) {
        try {
            String req = "update Pole set idResponsablePole=? , description=? where idPole=?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setString(1, idResponsablePole);
            pt.setString(2, description);
            pt.setString(3, idPole);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void supprimerPole(String idPole) {
        try {
            String req = "delete from Pole where idPole=?";
            PreparedStatement pt = conn.prepareStatement(req);
            pt.setString(1, idPole);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public List<Pole> getPoles() {
        Statement st;
        List<Pole> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Pole";
            ResultSet rs = st.executeQuery(req);
            ServiceUtilisateur su = new ServiceUtilisateur();
            while (rs.next()) {
                ls.add(new Pole(rs.getString(1), rs.getString(3), (Employe) su.getUtilisateur(rs.getString(2))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }

    public Pole getPole(String idPole) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "select * from Pole where idPole = '" + idPole + "'";
            ResultSet rs = st.executeQuery(req);
            ServiceUtilisateur su = new ServiceUtilisateur();
            while (rs.next()) {
                return new Pole(rs.getString(1), rs.getString(3), (Employe) su.getUtilisateur(rs.getString(2)));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public Pole getPoleParResponsablePole(String idResponsablePole) {
        Statement st;
        try {
            st = conn.createStatement();
            String req = "select * from Pole where idResponsablePole = '" + idResponsablePole + "'";
            ResultSet rs = st.executeQuery(req);
            ServiceUtilisateur su = new ServiceUtilisateur();
            while (rs.next()) {
                return new Pole(rs.getString(1), rs.getString(3), (Employe) su.getUtilisateur(rs.getString(2)));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public List<Pole> getPolesParMotCle(String motCle) {
        Statement st;
        List<Pole> ls = new ArrayList();
        try {
            st = conn.createStatement();
            String req = "select * from Pole where idPole like '%" + motCle + "%' or description like '%" + motCle + "%'";
            ResultSet rs = st.executeQuery(req);
            ServiceUtilisateur su = new ServiceUtilisateur();
            while (rs.next()) {
                ls.add(new Pole(rs.getString(1), rs.getString(3), (Employe) su.getUtilisateur(rs.getString(2))));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ls;
    }*/
}
