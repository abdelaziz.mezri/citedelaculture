/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mezri
 */
public class Espace {

    private String idEspace;
    private String description;
    private int capacite;
    private String type;
    private double prixDeLocation;

    

    
    public Espace(String idEspace, String description, int capacite, String type, double prixDeLocation) {
        this.idEspace = idEspace;
        this.description = description;
        this.capacite = capacite;
        this.type = type;
        this.prixDeLocation = prixDeLocation;
    }
    
    

    public String getIdEspace() {
        return idEspace;
    }

    public String getDescription() {
        return description;
    }

    public int getCapacite() {
        return capacite;
    }

    public String getType() {
        return type;
    }

    public double getPrixDeLocation() {
        return prixDeLocation;
    }

    
    public void setIdEspace(String idEspace) {
        this.idEspace = idEspace;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPrixDeLocation(double prixDeLocation) {
        this.prixDeLocation = prixDeLocation;
    }

   

    @Override
    public String toString() {
        return idEspace;
    }
}
