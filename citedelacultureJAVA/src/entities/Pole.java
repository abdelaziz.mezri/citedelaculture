/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Mezri
 */
public class Pole {

    private String idPole;
    private String description;
    private Utilisateur responsablePole;
    private List<Club> clubs;

    public Pole(String idPole, String description, Utilisateur responsablePole) {
        this.idPole = idPole;
        this.description = description;
        this.responsablePole = responsablePole;
    }

    public Pole() {
    }

    public String getIdPole() {
        return idPole;
    }

    public String getDescription() {
        return description;
    }

    public void setIdPole(String idPole) {
        this.idPole = idPole;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Club> getClubs() {
        return clubs;
    }

    public void setClubs(List<Club> clubs) {
        this.clubs = clubs;
    }

    @Override
    public String toString() {
        return idPole;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pole other = (Pole) obj;
        if (!Objects.equals(this.idPole, other.idPole)) {
            return false;
        }
        return true;
    }
}
