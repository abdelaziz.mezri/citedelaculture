/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Mezri
 */
public class Rating {

    int id;
    private int valeur;
    private String commentaire;
    private Evenement evenement;
    private Utilisateur visiteur;

    public Rating() {
    }

    public int getId() {
        return id;
    }

    public Rating(int id, int valeur, String commentaire, Evenement evenement, Utilisateur visiteur) {
        this.id = id;
        this.valeur = valeur;
        this.commentaire = commentaire;
        this.evenement = evenement;
        this.visiteur = visiteur;
    }

    public Rating(Evenement evenement, Utilisateur visiteur) {
        this.evenement = evenement;
        this.visiteur = visiteur;
    }

    public Rating(int valeur, String commentaire, Evenement evenement, Utilisateur visiteur) {
        this.valeur = valeur;
        this.commentaire = commentaire;
        this.evenement = evenement;
        this.visiteur = visiteur;
    }

    public int getValeur() {
        return valeur;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public Evenement getEvenement() {
        return evenement;
    }

    public Utilisateur getVisiteur() {
        return visiteur;
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public void setEvenement(Evenement evenement) {
        this.evenement = evenement;
    }

    public void setVisiteur(Utilisateur visiteur) {
        this.visiteur = visiteur;
    }

    @Override
    public String toString() {
        return "Rating{" + "valeur=" + valeur + ", commentaire=" + commentaire + ", evenement=" + evenement.getIdEvenement() + '}';
    }

}
