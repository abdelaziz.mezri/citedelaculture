/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.admin.gestiondesespaces.ajouter;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Callback;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import entities.Espace;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Pagination;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.VBox;
import javax.imageio.ImageIO;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;
import views.admin.gestiondesespaces.MainController;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class InterfaceajouterEspaceController implements Initializable {

    @FXML
    private JFXTextField tfidEspace;
    @FXML
    private JFXTextField tdCapacité;
    @FXML
    private JFXTextField tfPrixdeLocation;
    @FXML
    private JFXComboBox<String> comboTypeEspace;
    @FXML
    private JFXTextArea taDescription;
    @FXML
    private JFXButton btAjouter;
    @FXML
    private JFXButton btRetour;
    @FXML
    private JFXButton btselectionnerespace;
    private Pagination pagination;
    File[] affiches;
    @FXML
    private AnchorPane anchoraffiches;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        comboTypeEspace.setItems(FXCollections.observableArrayList("Opéra", "Theatre", "Club", "Cinéma"));
    }

    @FXML
    private void onActionAjouter(ActionEvent event) throws ParseException {

        Espace s;
        String idEspace = tfidEspace.getText();
        int capacité = Integer.parseInt(tdCapacité.getText());
        double prixlocation = Double.parseDouble(tfPrixdeLocation.getText());
        String description = taDescription.getText();
        String typeespace = (String) comboTypeEspace.getValue();
        //s = new Espace(idEspace, description, capacité, typeespace, prixlocation, Arrays.asList(affiches));
        //new services.ServiceEspace().ajouterEspace(s);
        MainController.stageAjouter.hide();
        TrayNotification tray = new TrayNotification();
        tray.setTitle("Succès");
        tray.setMessage("Votre espace est ajouté avec succés");
        tray.setAnimationType(AnimationType.SLIDE);
        tray.setNotificationType(NotificationType.SUCCESS);
        tray.showAndWait();

    }

    @FXML
    private void onRetourAction(ActionEvent event) {
        MainController.stageAjouter.hide();
    }

    @FXML
    private void onSelectioneEspaceAction(ActionEvent event) {

        openDirectoryChooser(new Stage());
    }

    private void openDirectoryChooser(Stage parent) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(parent);
        if (selectedDirectory != null) {
            FilenameFilter filterJpg = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".jpg");
                }
            };
            affiches = selectedDirectory.listFiles(filterJpg);
            openPageWindow();
        }
    }

    private void openPageWindow() {
        int numOfPage = affiches.length;
        pagination = new Pagination(numOfPage);
        pagination.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return createPage(pageIndex);
            }
        });
        AnchorPane.setTopAnchor(pagination, 10.0);
        AnchorPane.setRightAnchor(pagination, 10.0);
        AnchorPane.setBottomAnchor(pagination, 10.0);
        AnchorPane.setLeftAnchor(pagination, 10.0);
        anchoraffiches.getChildren().clear();
        anchoraffiches.getChildren().add(pagination);
    }

    public VBox createPage(int index) {
        ImageView imageView = new ImageView();
        File file = affiches[index];
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            WritableImage image = SwingFXUtils.toFXImage(bufferedImage, null);
            imageView.setImage(image);
            imageView.setFitWidth(300);
            imageView.setFitHeight(100);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
        } catch (IOException ex) {
            Logger.getLogger(InterfaceajouterEspaceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        VBox pageBox = new VBox();
        pageBox.getChildren().add(imageView);
        return pageBox;
    }

}
