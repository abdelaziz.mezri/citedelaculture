/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.admin.gestiondesespaces;

import entities.Espace;
import entities.Evenement;
import entities.Pole;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.Time;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import services.ServiceEspace;
import services.ServiceEvenement;
import static views.admin.gestiondesevenements.MainController.evenement;
import static views.admin.gestiondesevenements.MainController.stageAjouter;
import static views.admin.gestiondesevenements.MainController.stageModifier;

/**
 * FXML Controller class
 *
 * @author Mezri
 */
public class MainController implements Initializable {

    @FXML
    private TableView<Espace> tableEspaces;
    @FXML
    private TableColumn<Espace, String> colIdEspace;
    @FXML
    private TableColumn<Espace, Integer> colCapacite;
    @FXML
    private TableColumn<Espace, Double> colPrixLoc;
    @FXML
    private TableColumn<Espace, String> colType;
    @FXML
    private Button btAjouter;
    @FXML
    private Button btSupprimer;
    @FXML
    private Button btModifier;
    @FXML
    private TextField tfRechercher;

    public static Stage stageAjouter;
    public static Stage stageModifier;

    public static Espace espace;
    public List<Espace> espaces;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colIdEspace.setCellValueFactory(new PropertyValueFactory<Espace, String>("idEspace"));
        colCapacite.setCellValueFactory(new PropertyValueFactory<Espace, Integer>("capacite"));
        colPrixLoc.setCellValueFactory(new PropertyValueFactory<Espace, Double>("prixDeLocation"));
        colType.setCellValueFactory(new PropertyValueFactory<Espace, String>("type"));
        //espaces = new ServiceEspace().getEspaces();
        tableEspaces.getItems().addAll(FXCollections.observableList(espaces));
        btModifier.setVisible(false);
        btSupprimer.setVisible(false);
    }

    /*@FXML
    private void tableEspacesOnMouseClicked(MouseEvent event) {
        espace = new ServiceEspace().getEspace(tableEspaces.getSelectionModel().getSelectedItem().getIdEspace());
        if (espace != null) {
            btModifier.setVisible(true);
            btSupprimer.setVisible(true);
        }
    }

    @FXML
    private void btAjouterOnClick(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/admin/gestiondesespaces/ajouter/interfaceajouterEspace.fxml"));
            Scene scene = new Scene(root);
            stageAjouter = new Stage();
            stageAjouter.setTitle("Ajouter espace");
            stageAjouter.setResizable(false);
            stageAjouter.setScene(scene);
            stageAjouter.show();
        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btSupprimerOnClick(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Suppression d'espace");
        alert.setHeaderText("Supprimer " + espace.getIdEspace());
        alert.setContentText("Vous voulez vraiment supprimer l'espace " + espace.getIdEspace() + " ?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            new ServiceEspace().supprimerEspace(espace.getIdEspace());
            espaces = new ServiceEspace().getEspaces();
            tableEspaces.getItems().clear();
            tableEspaces.getItems().addAll(FXCollections.observableList(espaces));
        }
        if (result.get() == ButtonType.CANCEL) {
            alert.close();
        }
    }

    @FXML
    private void btModifierOnClick(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/admin/gestiondesespaces/modifier/interfacemodifierEspace.fxml"));
            Scene scene = new Scene(root);
            stageModifier = new Stage();
            stageModifier.setTitle("Modifier un espace");
            stageModifier.setResizable(false);
            stageModifier.setScene(scene);
            stageModifier.show();
        } catch (IOException ex) {
            Logger.getLogger(views.admin.gestiondesevenements.MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void tfRechecherOnKeyReleased(KeyEvent event) {
        btModifier.setVisible(false);
        btSupprimer.setVisible(false);
        espaces = new ServiceEspace().getEspacesParMotCle(tfRechercher.getText());
        tableEspaces.getItems().clear();
        tableEspaces.getItems().addAll(FXCollections.observableList(espaces));
    }*/

}
