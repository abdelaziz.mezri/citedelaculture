/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.admin.gestiondesespaces.modifier;

import views.admin.gestiondesespaces.ajouter.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Callback;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import entities.Espace;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Pagination;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.VBox;
import javax.imageio.ImageIO;
import services.ServiceEspace;
import views.admin.gestiondesespaces.MainController;
import views.admin.gestiondesevenements.modifier.ModifierEvenementController;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class InterfacemodifierEspaceController implements Initializable {

    @FXML
    private JFXTextField tfidEspace;
    @FXML
    private JFXTextField tdCapacité;
    @FXML
    private JFXTextField tfPrixdeLocation;
    @FXML
    private JFXComboBox<String> comboTypeEspace;
    @FXML
    private JFXTextArea taDescription;
    @FXML
    private JFXButton btRetour;
    @FXML
    private JFXButton btselectionnerespace;
    @FXML
    private AnchorPane anchoraffiches;
    @FXML
    private JFXButton btModifier;

    private Pagination pagination;
    private File[] affiches;
    Espace e = MainController.espace;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        comboTypeEspace.setItems(FXCollections.observableArrayList("Opéra", "Theatre", "Club", "Cinéma"));
        tfidEspace.setText(e.getIdEspace());
        tdCapacité.setText(e.getCapacite() + "");
        tfPrixdeLocation.setText(e.getPrixDeLocation() + "");
        comboTypeEspace.setValue(e.getType());
        taDescription.setText(e.getDescription());
        //affiches = new File[e.getListImagesEspace().size()];
        //e.getListImagesEspace().toArray(affiches);
        openPageWindow();
    }

    @FXML
    private void onRetourAction(ActionEvent event) {
        MainController.stageModifier.hide();
    }

    @FXML
    private void onSelectioneEspaceAction(ActionEvent event) {
        openDirectoryChooser(new Stage());
    }

    /*@FXML
    private void onActionModifier(ActionEvent event) {

        String idEspace = tfidEspace.getText();
        String description = taDescription.getText();
        int capacite = Integer.parseInt(tdCapacité.getText());
        String type = comboTypeEspace.getValue();
        Double prixDeLocation = Double.parseDouble(tfPrixdeLocation.getText());
        e = new Espace(idEspace, description, capacite, type, prixDeLocation, Arrays.asList(affiches));
        new ServiceEspace().modifierEspace(MainController.espace, e);
        MainController.stageModifier.hide();
    }*/

    private void openDirectoryChooser(Stage parent) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(parent);
        if (selectedDirectory != null) {
            FilenameFilter filterJpg = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".jpg");
                }
            };
            affiches = selectedDirectory.listFiles(filterJpg);
            openPageWindow();
        }
    }

    private void openPageWindow() {
        int numOfPage = affiches.length;
        pagination = new Pagination(numOfPage);
        pagination.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return createPage(pageIndex);
            }
        });
        AnchorPane.setTopAnchor(pagination, 10.0);
        AnchorPane.setRightAnchor(pagination, 10.0);
        AnchorPane.setBottomAnchor(pagination, 10.0);
        AnchorPane.setLeftAnchor(pagination, 10.0);
        anchoraffiches.getChildren().clear();
        anchoraffiches.getChildren().add(pagination);
    }

    public VBox createPage(int index) {
        ImageView imageView = new ImageView();
        File file = affiches[index];
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            javafx.scene.image.Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            imageView.setImage(image);
            imageView.setFitWidth(300);
            imageView.setFitHeight(200);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
        } catch (IOException ex) {
            Logger.getLogger(ModifierEvenementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        VBox pageBox = new VBox();
        pageBox.getChildren().add(imageView);
        return pageBox;
    }

}
