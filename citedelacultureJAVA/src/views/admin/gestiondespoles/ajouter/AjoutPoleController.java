/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.admin.gestiondespoles.ajouter;

import entities.Pole;
import entities.Utilisateur;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import services.ServiceEspace;
import services.ServicePole;
import services.ServiceUtilisateur;
import views.admin.gestiondespoles.ListePoleController;

/**
 * FXML Controller class
 *
 * @author Mezri
 */
public class AjoutPoleController implements Initializable {

    @FXML
    private TextField tfIdPole;
    @FXML
    private TextField taDescription;
    @FXML
    private Button btAjouter;
    @FXML
    private Button btAnnuler;
    @FXML
    private ComboBox<Utilisateur> cbResponsable;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //cbResponsable.setItems(FXCollections.observableArrayList(new ServiceUtilisateur().getResponsablesPoles()));
    }    

    /*@FXML
    private void onClickAjouter(ActionEvent event) {
        new ServicePole().ajouterPole(new Pole(tfIdPole.getText(), taDescription.getText(), cbResponsable.getValue()));
        ListePoleController.stageAjouter.hide();    
    }

    @FXML
    private void onClickAnnuler(ActionEvent event) {
        ListePoleController.stageAjouter.hide();
    }*/
    
}
