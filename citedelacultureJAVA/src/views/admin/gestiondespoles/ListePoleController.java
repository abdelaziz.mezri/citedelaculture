/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.admin.gestiondespoles;

import entities.Evenement;
import entities.Pole;
import entities.Utilisateur;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import services.ServiceEvenement;
import services.ServicePole;
import static views.admin.gestiondesevenements.MainController.evenement;

/**
 * FXML Controller class
 *
 * @author Mezri
 */
public class ListePoleController implements Initializable {

    @FXML
    private TableView<Pole> TablePole;
    @FXML
    private TableColumn<Pole, String> colNomPole;
    @FXML
    private TableColumn<Pole, Utilisateur> colResponsablePole;
    @FXML
    private Button btAjouter;
    @FXML
    private Button btSupprimer;
    @FXML
    private Button btModifier;
    @FXML
    private TextField tfRechercher;

    public static Stage stageAjouter;
    public static Stage stageModifier;

    public static Pole pole;
    public List<Pole> poles;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colNomPole.setCellValueFactory(new PropertyValueFactory<Pole, String>("idPole"));
        //colResponsablePole.setCellValueFactory(new PropertyValueFactory<Pole, ResponsablePole>("responsablePole"));
        //poles = new ServicePole().getPoles();
        TablePole.getItems().addAll(FXCollections.observableList(poles));
        btModifier.setVisible(false);
        btSupprimer.setVisible(false);
    }

    @FXML
    private void AjouterOnClick(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/admin/gestiondespoles/ajouter/AjoutPole.fxml"));
            Scene scene = new Scene(root);
            stageAjouter = new Stage();
            stageAjouter.setTitle("Ajouter un événement");
            stageAjouter.setResizable(false);
            stageAjouter.setScene(scene);
            stageAjouter.show();
        } catch (IOException ex) {
            Logger.getLogger(ListePoleController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*@FXML
    private void SupprimerOnClick(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Suppression pole");
        alert.setHeaderText("Supprimer " + pole.getIdPole());
        alert.setContentText("Vous voulez vraiment supprimer pole ?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            new ServicePole().supprimerPole(pole.getIdPole());
            poles = new ServicePole().getPoles();
            TablePole.getItems().clear();
            TablePole.getItems().addAll(FXCollections.observableList(poles));
        }
        if (result.get() == ButtonType.CANCEL) {
            alert.close();
        }
    }

    @FXML
    private void ModifierOnClick(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/admin/gestiondespoles/modifier/ModifierPole.fxml"));
            Scene scene = new Scene(root);
            stageModifier = new Stage();
            stageModifier.setTitle("Modifier un pole");
            stageModifier.setResizable(false);
            stageModifier.setScene(scene);
            stageModifier.show();
        } catch (IOException ex) {
            Logger.getLogger(ListePoleController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void TablePoleOnMouseClicked(MouseEvent event) {
        pole = new ServicePole().getPole(TablePole.getSelectionModel().getSelectedItem().getIdPole());
        if (pole != null) {
            btModifier.setVisible(true);
            btSupprimer.setVisible(true);
        }
    }

    @FXML
    private void tfRechercherOnKeyReleased(KeyEvent event) {
        btModifier.setVisible(false);
        btSupprimer.setVisible(false);
        poles = new ServicePole().getPolesParMotCle(tfRechercher.getText());
        TablePole.getItems().clear();
        TablePole.getItems().addAll(FXCollections.observableList(poles));
    }*/
}
