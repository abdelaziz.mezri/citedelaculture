/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.admin;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Mezri
 */
public class DashboardAdminController implements Initializable {

    @FXML
    private AnchorPane parent;
    @FXML
    private JFXButton btGestionDesPublications;
    @FXML
    private JFXButton btGestionDesEvenemets;
    @FXML
    private JFXButton btGestionDesPoles;
    @FXML
    private JFXButton btGestionDesEspaces;
    @FXML
    private JFXButton btGestionDesMessages;
    @FXML
    private JFXButton btGestionDesEmployes;
    @FXML
    private JFXButton btGestionDesVisiteurs;
    @FXML
    private JFXButton btGestionDesOrganismesExt;
    @FXML
    private JFXButton btStatistiques;
    @FXML
    private AnchorPane barrePane;
    @FXML
    private JFXButton btDeconnecter;
    @FXML
    private JFXButton btQuitter;
    @FXML
    private AnchorPane PagePane;

    AnchorPane publications, evenements, poles, espaces, messages, employes, visiteurs, organismesExt, statistiques;

    private double xOffset = 0;
    private double yOffset = 0;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        makeStageDrageable();
        try {
           // poles = FXMLLoader.load(getClass().getResource("/views/admin/gestiondespoles/ListePole.fxml"));
           // espaces = FXMLLoader.load(getClass().getResource("/views/admin/gestiondesespaces/Main.fxml"));
            evenements = FXMLLoader.load(getClass().getResource("/views/admin/gestiondesevenements/Main.fxml"));
            
        } catch (IOException ex) {
            Logger.getLogger(DashboardAdminController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btGestionDesPublicationsOnClick(ActionEvent event) {
    }

    @FXML
    private void btGestionDesEvenemetsOnClick(ActionEvent event) {
        setNode(evenements);
    }

    @FXML
    private void btGestionDesPolesOnClick(ActionEvent event) {
        setNode(poles);
    }

    @FXML
    private void btGestionDesEspacesOnClick(ActionEvent event) {
        setNode(espaces);
    }

    @FXML
    private void btQuitterOnClick(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Quitter");
        alert.setHeaderText("Quitter l'application");
        alert.setContentText("Vous voulez vraiment quitter ?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            System.exit(0);
        }
        if (result.get() == ButtonType.CANCEL) {
            alert.close();
        }
    }

    private void makeStageDrageable() {
        parent.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        parent.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                utiles.CiteDeLaCulture.stage.setX(event.getScreenX() - xOffset);
                utiles.CiteDeLaCulture.stage.setY(event.getScreenY() - yOffset);
                utiles.CiteDeLaCulture.stage.setOpacity(0.8f);
            }
        });
        parent.setOnDragDone((e) -> {
            utiles.CiteDeLaCulture.stage.setOpacity(1.0f);
        });
        parent.setOnMouseReleased((e) -> {
            utiles.CiteDeLaCulture.stage.setOpacity(1.0f);
        });
    }

    private void setNode(Node node) {
        PagePane.getChildren().clear();
        PagePane.getChildren().add((Node) node);

        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
    }
}
