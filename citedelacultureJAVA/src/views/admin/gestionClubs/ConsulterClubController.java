/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.admin.gestionClubs;

import entities.Club;
import entities.Pole;
import entities.Utilisateur;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import services.ServiceClub;
import services.ServicePole;
import views.admin.gestiondespoles.ListePoleController;
import static views.admin.gestiondespoles.ListePoleController.pole;
import static views.admin.gestiondespoles.ListePoleController.stageAjouter;
import static views.admin.gestiondespoles.ListePoleController.stageModifier;

/**
 * FXML Controller class
 *
 * @author Ahmed
 */
public class ConsulterClubController implements Initializable {

    @FXML
    private TableView<Club> TableClub;
    @FXML
    private TableColumn<Club, String> colNomClub;
    @FXML
    private TableColumn<Club, Double> colCotisation;
    @FXML
    private TableColumn<Pole, String> colIdPole;
    @FXML
    private TableColumn<Club, Utilisateur> colIdResponsableClub;
    @FXML
    private Button BtAjouter;
    @FXML
    private Button BtSupprimer;
    @FXML
    private Button Btmodifier;
    @FXML
    private Button BtConfirmer;
    @FXML
    private TextField tfRechercher;
    
    public static Stage stageAjouter;
    public static Stage stageModifier;
    
    public static Club club;
    public List<Club> clubs;
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colNomClub.setCellValueFactory(new PropertyValueFactory<Club, String>("idClub"));
        colCotisation.setCellValueFactory(new PropertyValueFactory<Club, Double>("cotisationAnnuelle"));
        colIdPole.setCellValueFactory(new PropertyValueFactory<Pole, String>("IdPole"));
        //colIdResponsableClub.setCellValueFactory(new PropertyValueFactory<Club, ResponsableClub>("responsableClub"));
        //clubs = new ServiceClub().getClubs();
        TableClub.getItems().addAll(FXCollections.observableArrayList(clubs));
        Btmodifier.setVisible(false);
        BtSupprimer.setVisible(false);
    }    
/*
    @FXML
    private void TableClubOnMouseClicked(MouseEvent event) {
        club = new ServiceClub().getClub(TableClub.getSelectionModel().getSelectedItem().getIdClub());
        if (pole != null) {
            Btmodifier.setVisible(true);
            BtSupprimer.setVisible(true);
        }
    }

    @FXML
    private void AjouterOnClick(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/admin/gestionClubs/ajouter/AjoutClub.fxml"));
            Scene scene = new Scene(root);
            stageAjouter = new Stage();
            stageAjouter.setTitle("Ajouter un Club");
            stageAjouter.setResizable(false);
            stageAjouter.setScene(scene);
            stageAjouter.show();
        } catch (IOException ex) {
            Logger.getLogger(ListePoleController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void SupprimerOnClick(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Suppression club");
        alert.setHeaderText("Supprimer " + club.getIdClub());
        alert.setContentText("Vous voulez vraiment supprimer Club ?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            new ServiceClub().supprimerClub(club.getIdClub());
            clubs = new ServiceClub().getClubs();
            TableClub.getItems().clear();
            TableClub.getItems().addAll(FXCollections.observableList(clubs));
        }
        if (result.get() == ButtonType.CANCEL) {
            alert.close();
        }
    }

    @FXML
    private void ModifierOnClick(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/admin/gestionClubs/modifier/ModifierClub.fxml"));
            Scene scene = new Scene(root);
            stageModifier = new Stage();
            stageModifier.setTitle("Modifier un Club");
            stageModifier.setResizable(false);
            stageModifier.setScene(scene);
            stageModifier.show();
        } catch (IOException ex) {
            Logger.getLogger(ListePoleController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void ConfirmerOnClick(ActionEvent event) {
    }

    @FXML
    private void tfRechercherOnKeyReleased(KeyEvent event) {
        Btmodifier.setVisible(false);
        BtSupprimer.setVisible(false);
        clubs = new ServiceClub().getClubsParPole(tfRechercher.getText());
        TableClub.getItems().clear();
        TableClub.getItems().addAll(FXCollections.observableList(clubs));
    }*/
    
}
