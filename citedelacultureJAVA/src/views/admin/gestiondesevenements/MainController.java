/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.admin.gestiondesevenements;

import entities.Espace;
import entities.Evenement;
import entities.Pole;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.StageStyle;
import services.ServiceEvenement;
import javafx.stage.Stage;
import utiles.CiteDeLaCulture;

/**
 * FXML Controller class
 *
 * @author Mezri
 */
public class MainController implements Initializable {

    @FXML
    private TableView<Evenement> tableEvenements;
    @FXML
    private TableColumn<Evenement, String> colNom;
    @FXML
    private TableColumn<Evenement, Integer> colId;
    @FXML
    private TableColumn<Evenement, Boolean> colConfirme;
    @FXML
    private Button btAjouter;
    @FXML
    private Button btSupprimer;
    @FXML
    private Button btModifier;
    @FXML
    private Button btConfirmer;
    @FXML
    private RadioButton rbTous;
    @FXML
    private ToggleGroup consulterTypeGroup;
    @FXML
    private RadioButton rbConfirmes;
    @FXML
    private RadioButton rbNonConfirmes;
    @FXML
    private TextField tfRechecher;

    public static Stage stageAjouter;
    public static Stage stageModifier;
    public static Stage stageConfirmer;

    public static Evenement evenement;
    public List<Evenement> evenements;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colNom.setCellValueFactory(new PropertyValueFactory<Evenement, String>("nom"));
        colId.setCellValueFactory(new PropertyValueFactory<Evenement, Integer>("idEvenement"));
        colConfirme.setCellValueFactory(new PropertyValueFactory<Evenement, Boolean>("confirme"));
        evenements = new ServiceEvenement().getSimpleEvenements();
        tableEvenements.getItems().addAll(FXCollections.observableList(evenements));
        btModifier.setVisible(false);
        btSupprimer.setVisible(false);
        btConfirmer.setVisible(false);
    }

    @FXML
    private void btAjouterOnClick(ActionEvent event) {
        /*try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/admin/gestiondesevenements/ajouter/AjouterEvenement.fxml"));
            Scene scene = new Scene(root);
            CiteDeLaCulture.stage.setScene(scene);
            CiteDeLaCulture.stage.show();
            Parent root = FXMLLoader.load(getClass().getResource("/views/admin/gestiondesevenements/ajouter/AjouterEvenement.fxml"));
            Scene scene = new Scene(root);
            stageAjouter = new Stage();
            stageAjouter.setTitle("Ajouter un pole");
            stageAjouter.setResizable(false);
            stageAjouter.setScene(scene);
            stageAjouter.show();
        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }

    @FXML
    private void btSupprimerOnClick(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Suppression d'événement");
        alert.setHeaderText("Supprimer " + evenement.getIdEvenement());
        alert.setContentText("Vous voulez vraiment supprimer l'événement " + evenement.getIdEvenement() + " ?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            new ServiceEvenement().supprimerEvenement(evenement.getIdEvenement() + "");
            evenements = new ServiceEvenement().getSimpleEvenements();
            actuliserTable();
        }
        if (result.get() == ButtonType.CANCEL) {
            alert.close();
        }
    }

    @FXML
    private void btModifierOnClick(ActionEvent event) {
        /*try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/admin/gestiondesevenements/modifier/ModifierEvenement.fxml"));
            Scene scene = new Scene(root);
            stageModifier = new Stage();
            stageModifier.setTitle("Modifier un événement");
            stageModifier.setResizable(false);
            stageModifier.setScene(scene);
            stageModifier.show();
        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }

    @FXML
    private void btConfirmerOnClick(ActionEvent event) {
        new ServiceEvenement().confirmerEvenement(evenement.getIdEvenement());
        btConfirmer.setVisible(false);
        evenements = new ServiceEvenement().getSimpleEvenements();
        actuliserTable();
    }

    @FXML
    private void rbTousOnSelect(ActionEvent event) {
        btModifier.setVisible(false);
        btSupprimer.setVisible(false);
        btConfirmer.setVisible(false);
        evenements = new ServiceEvenement().getSimpleEvenements();
        actuliserTable();
    }

    @FXML
    private void rbConfirmesOnSelect(ActionEvent event) {
        btModifier.setVisible(false);
        btSupprimer.setVisible(false);
        btConfirmer.setVisible(false);
        evenements = new ServiceEvenement().getSimpleEvenementsConfirmes();
        actuliserTable();
    }

    @FXML
    private void rbNonConfirmesOnSelect(ActionEvent event) {
        btModifier.setVisible(false);
        btSupprimer.setVisible(false);
        btConfirmer.setVisible(false);
        evenements = new ServiceEvenement().getSimpleEvenementsNonConfirmes();
        actuliserTable();

    }

    @FXML
    private void tfRechecherOnKeyReleased(KeyEvent event) {
        btModifier.setVisible(false);
        btSupprimer.setVisible(false);
        btConfirmer.setVisible(false);
        evenements = new ServiceEvenement().getSimpleEvenementsParMotCle(tfRechecher.getText());
        actuliserTable();
    }

    @FXML
    private void tableEvenementsOnMouseClicked(javafx.scene.input.MouseEvent event) {
        evenement = new ServiceEvenement().getEvenement(tableEvenements.getSelectionModel().getSelectedItem().getIdEvenement() + "");
        if (evenement != null) {
            btModifier.setVisible(true);
            btSupprimer.setVisible(true);
            if (evenement.isConfirme()) {
                btConfirmer.setVisible(false);
            } else {
                btConfirmer.setVisible(true);
            }
        }
    }

    public void actuliserTable() {
        tableEvenements.getItems().clear();
        tableEvenements.getItems().addAll(FXCollections.observableList(evenements));
    }
}
