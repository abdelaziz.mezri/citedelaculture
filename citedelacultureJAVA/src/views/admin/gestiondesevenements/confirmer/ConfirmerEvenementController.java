/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.admin.gestiondesevenements.confirmer;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import entities.Espace;
import entities.Evenement;
import java.net.URL;
import java.sql.Date;
import java.sql.Time;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import services.ServiceEspace;
import services.ServiceEvenement;
import views.admin.gestiondesevenements.MainController;

/**
 * FXML Controller class
 *
 * @author Mezri
 */
public class ConfirmerEvenementController implements Initializable {

    @FXML
    private JFXTextField tfNomEvenement;
    @FXML
    private JFXButton btConfirmer;
    @FXML
    private JFXButton btAnnuler;
    @FXML
    private JFXDatePicker date;
    @FXML
    private JFXTimePicker temps;
    @FXML
    private JFXComboBox<Espace> cbEspace;

    Evenement e = MainController.evenement;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        /*tfNomEvenement.setText(e.getIdEvenement());
        date.setValue(e.getDate().toLocalDate());
        temps.setValue(e.getTemps().toLocalTime());
        cbEspace.setItems(FXCollections.observableArrayList(new ServiceEspace().getEspaceParCapacite(e.getNbMaxPlaces())));
    */}

    /*@FXML
    private void btConfirmerOnClick(ActionEvent event) {
        Date d = Date.valueOf(date.getValue());
        Time t = Time.valueOf(temps.getValue());
        Espace espace = new Espace(cbEspace.getValue().getIdEspace());
        new ServiceEvenement().confirmerEvenement(e.getIdEvenement(), d, t, espace);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Modification d'un évenement");
        alert.setHeaderText("Modification de l'évenement " + tfNomEvenement.getText());
        alert.setContentText("l'événément " + tfNomEvenement.getText() + " est modifié avec succés");
        MainController.stageConfirmer.hide();
    }

    @FXML
    private void btAnnulerOnClick(ActionEvent event) {
        MainController.stageConfirmer.hide();
    }*/

}
