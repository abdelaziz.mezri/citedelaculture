/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.admin.gestiondesevenements.ajouter;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import entities.Espace;
import entities.Evenement;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Pagination;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import services.ServiceEspace;
import services.ServiceEvenement;
import views.admin.gestiondesevenements.MainController;

/**
 * FXML Controller class
 *
 * @author Mezri
 */
public class AjouterEvenementController implements Initializable {

    @FXML
    private JFXTextField tfNomEvenement;
    @FXML
    private JFXTextArea taDescription;
    @FXML
    private JFXCheckBox chbSpectacle;
    @FXML
    private JFXButton btParcourirAffiches;
    @FXML
    private JFXTextField tfGenre;
    @FXML
    private JFXComboBox<String> cbNationalite;
    @FXML
    private JFXTextField tfActeurs;
    @FXML
    private JFXTextField tfLienTrailer;
    @FXML
    private Spinner<Integer> spNbMax;
    @FXML
    private Spinner<Double> spPrix;
    @FXML
    private JFXButton btConfirmer;
    @FXML
    private JFXButton btAnnuler;
    @FXML
    private AnchorPane anchorAffiches;
    @FXML
    private JFXDatePicker date;
    @FXML
    private JFXTimePicker temps;
    @FXML
    private JFXComboBox<Espace> cbEspace;

    private Pagination pagination;
    File[] affiches;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tfGenre.setVisible(false);
        tfActeurs.setVisible(false);
        tfLienTrailer.setVisible(false);
        //cbEspace.setItems(FXCollections.observableArrayList(new ServiceEspace().getEspaces()));
        List<String> countries = new ArrayList();
        Arrays.asList(Locale.getAvailableLocales()).stream().forEach(c -> {
            if (c.getDisplayCountry() != "") {
                countries.add(c.getCountry() + ", " + c.getDisplayCountry());
            }
        });
        cbNationalite.setItems(FXCollections.observableArrayList(countries));
        SpinnerValueFactory<Double> valueFactoryPrix = new SpinnerValueFactory.DoubleSpinnerValueFactory(0, 100, 0);
        this.spPrix.setValueFactory(valueFactoryPrix);
        SpinnerValueFactory<Integer> valueFactoryNbMax = new SpinnerValueFactory.IntegerSpinnerValueFactory(30, 1000, 30);
        this.spNbMax.setValueFactory(valueFactoryNbMax);
    }

    @FXML
    private void btParcourirAffichesOnClick(ActionEvent event) {
        openDirectoryChooser(new Stage());
    }

    @FXML
    private void btConfirmerOnClick(ActionEvent event) throws ParseException {
        //ajouterEvenement();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Ajout d'un évenement");
        alert.setHeaderText("Ajout de l'évenement " + tfNomEvenement.getText());
        alert.setContentText("l'événément " + tfNomEvenement.getText() + " est ajouté avec succés");
        MainController.stageAjouter.hide();
    }

    @FXML
    private void btAnnulerOnClick(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Quitter l'ajout");
        alert.setHeaderText("Quitter l'ajout");
        alert.setContentText("Vous voulez vraiment abandonner l'ajout ?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            MainController.stageAjouter.hide();
        }
        if (result.get() == ButtonType.CANCEL) {
            alert.close();
        }
    }

    @FXML
    private void chbSpectacleChange(ActionEvent event) {
        if (chbSpectacle.isSelected()) {
            tfGenre.setVisible(true);
            tfActeurs.setVisible(true);
            tfLienTrailer.setVisible(true);
        } else {
            tfGenre.setVisible(false);
            tfActeurs.setVisible(false);
            tfLienTrailer.setVisible(false);
            tfGenre.setText(null);
            tfActeurs.setText(null);
            tfLienTrailer.setText(null);
        }
    }

    private void openDirectoryChooser(Stage parent) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(parent);
        if (selectedDirectory != null) {
            FilenameFilter filterJpg = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".jpg");
                }
            };
            affiches = selectedDirectory.listFiles(filterJpg);
            openPageWindow();
        }
    }

    private void openPageWindow() {
        int numOfPage = affiches.length;
        pagination = new Pagination(numOfPage);
        pagination.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return createPage(pageIndex);
            }
        });
        AnchorPane.setTopAnchor(pagination, 10.0);
        AnchorPane.setRightAnchor(pagination, 10.0);
        AnchorPane.setBottomAnchor(pagination, 10.0);
        AnchorPane.setLeftAnchor(pagination, 10.0);
        anchorAffiches.getChildren().clear();
        anchorAffiches.getChildren().add(pagination);
    }

    public VBox createPage(int index) {
        ImageView imageView = new ImageView();
        File file = affiches[index];
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            imageView.setImage(image);
            imageView.setFitWidth(300);
            imageView.setFitHeight(200);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
        } catch (IOException ex) {
            Logger.getLogger(AjouterEvenementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        VBox pageBox = new VBox();
        pageBox.getChildren().add(imageView);
        return pageBox;
    }

    /*public void ajouterEvenement() {
        Evenement e;
        if (chbSpectacle.isSelected()) {
            String nom = tfNomEvenement.getText();
            String description = taDescription.getText();
            String nationalite = cbNationalite.getValue();
            Date d = Date.valueOf(date.getValue());
            Time t = Time.valueOf(temps.getValue());
            Espace espace = new Espace(cbEspace.getValue().getIdEspace());
            String genre = tfGenre.getText();
            String acteurs = tfActeurs.getText();
            String lienTrailer = tfLienTrailer.getText();
            Double prix = (double) spPrix.getValue();
            int nbMaxPlaces = (int) spNbMax.getValue();
            e = new Spectacle(nom, description, nationalite, nbMaxPlaces, prix, true, d, t, espace, Arrays.asList(affiches), genre, acteurs, lienTrailer);
        } else {
            String nom = tfNomEvenement.getText();
            String description = taDescription.getText();
            String nationalite = cbNationalite.getValue();
            Date d = Date.valueOf(date.getValue());
            Time t = Time.valueOf(temps.getValue());
            Espace espace = new Espace(cbEspace.getValue().getIdEspace());
            Double prix = (double) spPrix.getValue();
            int nbMaxPlaces = (int) spNbMax.getValue();
            e = new Evenement(nom, description, nationalite, nbMaxPlaces, prix, true, d, t, espace, Arrays.asList(affiches));
        }
        new ServiceEvenement().ajouterEvenement(e);
    }

    @FXML
    private void spNbMaxOnKeyPressed(KeyEvent event) {
        cbEspace.getItems().clear();
        cbEspace.setItems(FXCollections.observableArrayList(new ServiceEspace().getEspaceParCapacite((int) spNbMax.getValue())));
    }

    @FXML
    private void spNbMaxOnMousePressed(MouseEvent event) {
        cbEspace.getItems().clear();
        cbEspace.setItems(FXCollections.observableArrayList(new ServiceEspace().getEspaceParCapacite((int) spNbMax.getValue())));
    }*/
}
