/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.login;

import com.jfoenix.controls.JFXButton;
import entities.Utilisateur;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import services.ServiceUtilisateur;
import utiles.CiteDeLaCulture;
import static views.login.LoginUtilisateurController.u;

/**
 * FXML Controller class
 *
 * @author Mezri
 */
public class LoginVisiteurController implements Initializable {

    @FXML
    private TextField tfEmail;
    @FXML
    private JFXButton btAuthentifier;

    public static Utilisateur visiteur;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    /*@FXML
    private void authentifier(ActionEvent event) {
        ServiceVisiteur sv = new ServiceVisiteur();
        visiteur = sv.getVisiteur(tfEmail.getText());
        if (visiteur == null) {
            Visiteur vis = new Visiteur(tfEmail.getText());
            sv.ajouterVisiteur(new Visiteur(tfEmail.getText()));
            visiteur = vis;
            try {
                Parent root = FXMLLoader.load(getClass().getResource("/views/visiteur/DashboardVisiteur.fxml"));
                Scene scene = new Scene(root);
                CiteDeLaCulture.stage.setScene(scene);
                CiteDeLaCulture.stage.show();
            } catch (IOException ex) {
                Logger.getLogger(LoginUtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                Parent root = FXMLLoader.load(getClass().getResource("/views/visiteur/DashboardVisiteur.fxml"));
                Scene scene = new Scene(root);
                CiteDeLaCulture.stage.setScene(scene);
                CiteDeLaCulture.stage.show();
            } catch (IOException ex) {
                Logger.getLogger(LoginUtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }*/

}
