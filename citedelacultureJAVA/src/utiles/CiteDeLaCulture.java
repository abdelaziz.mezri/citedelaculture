package utiles;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import services.ServiceEvenement;

/**
 *
 * @author Mezri
 */
public class CiteDeLaCulture extends Application {

    public static Stage stage = null;

    @Override
    public void start(Stage stage) throws Exception {
        //Parent root = FXMLLoader.load(getClass().getResource("/citedelaculture/views/admin/DashboardAdmin.fxml"));
        //Parent root = FXMLLoader.load(getClass().getResource("/views/admin/DashboardAdmin.fxml"));
        //Parent root = FXMLLoader.load(getClass().getResource("/views/visiteur/consulter/consulterEvenements.fxml"));
        Parent root = FXMLLoader.load(getClass().getResource("/views/login/Main.fxml"));
        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        stage.initStyle(StageStyle.TRANSPARENT);
        this.stage = stage;
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
