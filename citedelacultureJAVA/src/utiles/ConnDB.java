/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utiles;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Mezri
 */
public class ConnDB {

    private static String url = "jdbc:mysql://localhost:3306/cite";
    private static String user = "root";
    private static String passwd = "";

    private Connection conn;

    private static ConnDB inst;

    private ConnDB() {
        try {
            conn = DriverManager.getConnection(url, user, passwd);
            System.out.println("connection établie");
        } catch (SQLException ex) {
            System.err.println("erreur = " + ex.getMessage());
        }
    }

    public static ConnDB getInstance() {
        if (inst == null) {
            inst = new ConnDB();
        }
        return inst;
    }

    public Connection getConn() {
        return conn;
    }

}
