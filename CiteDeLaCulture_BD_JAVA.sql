-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 27 fév. 2019 à 20:18
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `citedelaculture`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnement`
--

DROP TABLE IF EXISTS `abonnement`;
CREATE TABLE IF NOT EXISTS `abonnement` (
  `idMembreClub` varchar(255) NOT NULL,
  `idClub` varchar(255) NOT NULL,
  `dateDebutAbonnement` date NOT NULL,
  `dateFinAbonnement` date NOT NULL,
  PRIMARY KEY (`idMembreClub`,`idClub`),
  KEY `abonnement_ibfk_1` (`idClub`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `club`
--

DROP TABLE IF EXISTS `club`;
CREATE TABLE IF NOT EXISTS `club` (
  `idClub` varchar(255) NOT NULL,
  `idPole` varchar(255) NOT NULL,
  `idResponsableClub` varchar(255) NOT NULL,
  `logo` longblob,
  `description` varchar(255) DEFAULT NULL,
  `cotisationAnnuelle` double DEFAULT NULL,
  PRIMARY KEY (`idClub`),
  KEY `idPole` (`idPole`),
  KEY `club_ibfk_1` (`idResponsableClub`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `idContact` int(11) NOT NULL AUTO_INCREMENT,
  `idEmploye` varchar(255) NOT NULL,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `numTel` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idContact`),
  KEY `idEmploye` (`idEmploye`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `espace`
--

DROP TABLE IF EXISTS `espace`;
CREATE TABLE IF NOT EXISTS `espace` (
  `idEspace` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `capacite` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `prixDeLocation` float NOT NULL,
  PRIMARY KEY (`idEspace`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `espace`
--

INSERT INTO `espace` (`idEspace`, `description`, `capacite`, `type`, `prixDeLocation`) VALUES
('a', 'cc', 3000, 'Salle', 200);

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

DROP TABLE IF EXISTS `evenement`;
CREATE TABLE IF NOT EXISTS `evenement` (
  `idEvenement` varchar(255) NOT NULL,
  `idPoleOrganisateur` varchar(255) DEFAULT NULL,
  `idEspace` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `nationalite` varchar(255) NOT NULL,
  `prix` double NOT NULL,
  `confirme` tinyint(1) DEFAULT NULL,
  `date` datetime NOT NULL,
  `genre` varchar(255) DEFAULT NULL,
  `acteurs` varchar(255) DEFAULT NULL,
  `lienTrailer` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`idEvenement`),
  KEY `evenement_ibfk_1` (`idEspace`),
  KEY `idOrganisateur` (`idPoleOrganisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `evenement`
--

INSERT INTO `evenement` (`idEvenement`, `idPoleOrganisateur`, `idEspace`, `description`, `nationalite`, `prix`, `confirme`, `date`, `genre`, `acteurs`, `lienTrailer`, `type`) VALUES
('test', NULL, 'a', 'a', 'b', 12, 1, '2019-02-19 09:30:00', NULL, NULL, NULL, 'Evenement');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `idImage` int(11) NOT NULL AUTO_INCREMENT,
  `idEspace` varchar(255) DEFAULT NULL,
  `idRevuDePress` int(11) DEFAULT NULL,
  `idEvenement` varchar(255) DEFAULT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `image` longblob NOT NULL,
  PRIMARY KEY (`idImage`),
  UNIQUE KEY `titre` (`titre`),
  KEY `idEspace` (`idEspace`),
  KEY `idRevuDePress` (`idRevuDePress`),
  KEY `idEvenement` (`idEvenement`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `idMessage` int(11) NOT NULL AUTO_INCREMENT,
  `idUtilisateurEmetteur` varchar(255) DEFAULT NULL,
  `idVisiteurEmetteur` varchar(255) DEFAULT NULL,
  `idDestinataire` varchar(255) NOT NULL,
  `lu` tinyint(1) NOT NULL,
  `objet` varchar(255) NOT NULL,
  `contenu` varchar(255) NOT NULL,
  `dateMessage` date NOT NULL,
  PRIMARY KEY (`idMessage`),
  KEY `idDestinataire` (`idDestinataire`),
  KEY `idEmetteur` (`idUtilisateurEmetteur`),
  KEY `idVisiteur` (`idVisiteurEmetteur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

DROP TABLE IF EXISTS `note`;
CREATE TABLE IF NOT EXISTS `note` (
  `idNote` int(11) NOT NULL AUTO_INCREMENT,
  `idEmploye` varchar(255) NOT NULL,
  `titre` varchar(25) NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idNote`),
  KEY `idEmploye` (`idEmploye`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `idNotification` int(11) NOT NULL AUTO_INCREMENT,
  `idDestinataire` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `vu` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idNotification`),
  KEY `idDestinataire` (`idDestinataire`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `organisme`
--

DROP TABLE IF EXISTS `organisme`;
CREATE TABLE IF NOT EXISTS `organisme` (
  `idOrganisme` varchar(255) NOT NULL,
  `idResponsableOrganismeExterne` varchar(255) DEFAULT NULL,
  `idPole` varchar(255) DEFAULT NULL,
  `logo` longblob,
  `description` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`idOrganisme`),
  UNIQUE KEY `nom` (`idOrganisme`),
  KEY `idResponsableOrganisme` (`idResponsableOrganismeExterne`),
  KEY `idPole` (`idPole`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `pole`
--

DROP TABLE IF EXISTS `pole`;
CREATE TABLE IF NOT EXISTS `pole` (
  `idPole` varchar(255) NOT NULL,
  `idResponsablePole` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`idPole`),
  KEY `idResponsablePole` (`idResponsablePole`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `publication`
--

DROP TABLE IF EXISTS `publication`;
CREATE TABLE IF NOT EXISTS `publication` (
  `idPublication` int(11) NOT NULL AUTO_INCREMENT,
  `idRedacteur` varchar(255) DEFAULT NULL,
  `titre` varchar(255) NOT NULL,
  `contenu` varchar(255) NOT NULL,
  `dateDePublication` date NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `fichier` longblob,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`idPublication`),
  KEY `idRedacteur` (`idRedacteur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `rating`
--

DROP TABLE IF EXISTS `rating`;
CREATE TABLE IF NOT EXISTS `rating` (
  `idEvenement` varchar(255) NOT NULL,
  `idVisiteur` varchar(255) NOT NULL,
  `valeur` int(11) NOT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idEvenement`,`idVisiteur`),
  KEY `idVisiteur` (`idVisiteur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `reservationespace`
--

DROP TABLE IF EXISTS `reservationespace`;
CREATE TABLE IF NOT EXISTS `reservationespace` (
  `idReservationEspace` int(11) NOT NULL AUTO_INCREMENT,
  `idUtilisateurClient` varchar(255) DEFAULT NULL,
  `idVisiteurClient` varchar(255) DEFAULT NULL,
  `idEspace` varchar(255) NOT NULL,
  `accepte` tinyint(1) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`idReservationEspace`),
  KEY `idUtilisateur` (`idUtilisateurClient`),
  KEY `idVisiteur` (`idVisiteurClient`),
  KEY `idEspace` (`idEspace`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tache`
--

DROP TABLE IF EXISTS `tache`;
CREATE TABLE IF NOT EXISTS `tache` (
  `idTache` int(11) NOT NULL AUTO_INCREMENT,
  `idEmploye` varchar(255) NOT NULL,
  `termine` tinyint(1) NOT NULL,
  `texte` varchar(150) NOT NULL,
  PRIMARY KEY (`idTache`),
  KEY `idEmploye` (`idEmploye`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `idEvenement` varchar(255) NOT NULL,
  `idVisiteur` varchar(255) NOT NULL,
  `nbPlaces` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`idEvenement`,`idVisiteur`),
  KEY `idVisiteur` (`idVisiteur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `idUtilisateur` varchar(255) NOT NULL,
  `image` longblob,
  `motDePasse` varchar(255) NOT NULL,
  `cin` varchar(255) DEFAULT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `numTel` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `dateDeNaissance` date NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`idUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idUtilisateur`, `image`, `motDePasse`, `cin`, `nom`, `prenom`, `email`, `numTel`, `adresse`, `dateDeNaissance`, `type`) VALUES
('admin', NULL, 'admin', NULL, 'a', 'a', 'a', 'a', 'a', '2019-02-06', 'Employe');

-- --------------------------------------------------------

--
-- Structure de la table `visiteur`
--

DROP TABLE IF EXISTS `visiteur`;
CREATE TABLE IF NOT EXISTS `visiteur` (
  `idVisiteur` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  PRIMARY KEY (`idVisiteur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `abonnement`
--
ALTER TABLE `abonnement`
  ADD CONSTRAINT `abonnement_ibfk_2` FOREIGN KEY (`idMembreClub`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `abonnement_ibfk_3` FOREIGN KEY (`idClub`) REFERENCES `club` (`idClub`);

--
-- Contraintes pour la table `club`
--
ALTER TABLE `club`
  ADD CONSTRAINT `club_ibfk_1` FOREIGN KEY (`idResponsableClub`) REFERENCES `utilisateur` (`idUtilisateur`) ON UPDATE CASCADE,
  ADD CONSTRAINT `club_ibfk_5` FOREIGN KEY (`idPole`) REFERENCES `pole` (`idPole`);

--
-- Contraintes pour la table `contact`
--
ALTER TABLE `contact`
  ADD CONSTRAINT `contact_ibfk_1` FOREIGN KEY (`idEmploye`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD CONSTRAINT `evenement_ibfk_2` FOREIGN KEY (`idPoleOrganisateur`) REFERENCES `pole` (`idPole`) ON UPDATE CASCADE,
  ADD CONSTRAINT `evenement_ibfk_3` FOREIGN KEY (`idEspace`) REFERENCES `espace` (`idEspace`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`idEspace`) REFERENCES `espace` (`idEspace`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `image_ibfk_2` FOREIGN KEY (`idEvenement`) REFERENCES `evenement` (`idEvenement`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`idDestinataire`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`idUtilisateurEmetteur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_ibfk_3` FOREIGN KEY (`idVisiteurEmetteur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `note_ibfk_1` FOREIGN KEY (`idEmploye`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`idDestinataire`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `organisme`
--
ALTER TABLE `organisme`
  ADD CONSTRAINT `organisme_ibfk_1` FOREIGN KEY (`idResponsableOrganismeExterne`) REFERENCES `utilisateur` (`idUtilisateur`) ON UPDATE CASCADE,
  ADD CONSTRAINT `organisme_ibfk_5` FOREIGN KEY (`idPole`) REFERENCES `pole` (`idPole`);

--
-- Contraintes pour la table `pole`
--
ALTER TABLE `pole`
  ADD CONSTRAINT `pole_ibfk_1` FOREIGN KEY (`idResponsablePole`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `publication`
--
ALTER TABLE `publication`
  ADD CONSTRAINT `publication_ibfk_1` FOREIGN KEY (`idRedacteur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `rating_ibfk_2` FOREIGN KEY (`idVisiteur`) REFERENCES `visiteur` (`idVisiteur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rating_ibfk_3` FOREIGN KEY (`idEvenement`) REFERENCES `evenement` (`idEvenement`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reservationespace`
--
ALTER TABLE `reservationespace`
  ADD CONSTRAINT `reservationespace_ibfk_1` FOREIGN KEY (`idUtilisateurClient`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservationespace_ibfk_2` FOREIGN KEY (`idVisiteurClient`) REFERENCES `visiteur` (`idVisiteur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservationespace_ibfk_3` FOREIGN KEY (`idEspace`) REFERENCES `espace` (`idEspace`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `tache`
--
ALTER TABLE `tache`
  ADD CONSTRAINT `tache_ibfk_1` FOREIGN KEY (`idEmploye`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `ticket_ibfk_2` FOREIGN KEY (`idVisiteur`) REFERENCES `visiteur` (`idVisiteur`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ticket_ibfk_3` FOREIGN KEY (`idEvenement`) REFERENCES `evenement` (`idEvenement`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
